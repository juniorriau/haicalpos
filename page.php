<?php
define( '_IEXEC', 1 );
session_start();

error_reporting(E_ALL);

//Koneksi database
include('core/cukang.inc.php');
//Library
include('core/core.php');


$p = paramDecrypt($_REQUEST['p']);

//Logout
if( $p=='logout' ){
	logout();
} 

//Cek Kondisi Login
if(!isLoggedIn()){	
	header('Location: home.php');
	die();
}

//User ID & Username
$userid 		= $_SESSION['userid'];
$username 		= ucwords( $_SESSION['fullname'] );
$foto	 		= $_SESSION['foto'];
$jabatan 		= $_SESSION['jabatan'];

$query  = "
	SELECT 
		c_nama
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
} else {
	$c_nama			= '';
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="robots" content="noindex">
		<title><?php echo strtoupper($c_nama)?> POSIS | DASBOARD</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico" />
			
		<!--============CSS=============-->
		<!-- Bootstrap 3.3.5 -->    
		<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- dataTables -->    
		<link type="text/css" rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
		<!-- Font Awesome -->
		<link type="text/css" rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
		<!-- Datepicker -->
		<link type="text/css" rel="stylesheet" href="plugins/datepicker/datepicker3.css">
		<!-- Theme style -->
		<link type="text/css" rel="stylesheet" href="dist/css/AdminLTE.min.css">    
		<!-- AdminLTE Skins. Choose a skin from the css/skins
			 folder instead of downloading all of them to reduce the load. -->
		<link type="text/css" rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
		
		<!--============JavaScript=============-->
		<!-- jQuery 2.1.4 -->
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- dataTables -->
		<script type="text/javascript"  src="plugins/datatables/jquery.dataTables.js"></script>
		<script type="text/javascript"  src="plugins/datatables/dataTables.bootstrap.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<!-- Autocomplete -->
		<script type="text/javascript" src="plugins/autocomplete/jquery.autocomplete.js"></script>
		<!-- Datepicker -->
		<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
		<!-- Inputmask Numeric -->
		<script type="text/javascript" src="plugins/input-mask/jquery.inputmask.js"></script>
		<script type="text/javascript" src="plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>
		<!-- Slimscroll -->
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- AdminLTE App -->
		<script type="text/javascript" src="dist/js/app.min.js"></script>
		<!-- Helper Function -->
		<script type="text/javascript" src="core/helper_func.js"></script>
	</head>
  
	<body class="hold-transition skin-purple sidebar-mini fixed">
		<div class="wrapper">
			<?php 
			include('inc/header.inc.php');
			
			// load side menu tergantung user
			include('inc/side_menu.inc.php');
			?>      
			<!-- content wrapper -->
			<div class="content-wrapper">
				<!-- content header -->
				<section class="content-header">
					<h1><?php echo ucwords($p);?></h1>
				</section>
				
				<!-- Main content -->
				<section class="content">
				<?php 
				$page = $p.'.php'; 
				// check file exist?					
				if (!file_exists('modules/'.$p.'/'.$page)) {   						
					error_404();
				} else {
					if(cek_hak_akses($jabatan, $p)===TRUE){
						include('modules/'.$p.'/'.$page);
					} else {
						access_denied();
					}				
				}
				?>
				</section>
				
			</div> <!-- content wrapper -->
			<?php include('inc/footer.inc.php');?>
		</div>	
		
		<!--============JavaScript=============-->
		
		
		
	</body>
</html>
