<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$type				= $_POST['type'];
	
	//data pembelian	
	$pb_kode			= $_POST['kodebeli'];
	$pb_suplier			= $_POST['suplier'];
	$pb_nota			= $_POST['nota'];
	$pb_jumlah			= $_POST['jtotal'];
	$pb_persendiskon	= $_POST['persendiskon'];
	$pb_diskon			= $_POST['diskon'];
	$pb_persenppn		= $_POST['persenppn'];
	$pb_ppn				= $_POST['ppn'];
	$pb_total			= $_POST['gtotal'];
	$pb_bayar			= $_POST['bayar'];
	$pb_sisa_bayar		= $_POST['sisabayar'];
	$pb_user_id			= $_POST['userid'];
		
	//data pembelian detail
	$pbd_id				= $_POST['pbdid'];
	$pbd_p_k_id			= $_POST['kategori'];
	$pbd_p_kode			= $_POST['kodeprod'];
	$pbd_p_nama			= $_POST['nama'];
	$pbd_p_harga_beli	= $_POST['hargabeli'];
	$pbd_p_satuan		= $_POST['satuan'];
	$pbd_p_tgl_masuk	= date("Y-m-d");
	$pbd_p_tgl_expire	= $_POST['tglexpire'];
	$pbd_beli			= $_POST['beli'];
	$pbd_jumlah			= $_POST['jumlah'];
	
	//data produk
	$p_stok				= $_POST['stok'];
	
	switch ($type) {
		case 'lunasi':									
				//Update ke pembelian
				mysql_query("
					UPDATE  
						pembelian 
					SET
						pb_tanggal		= NOW(),
						pb_status		= 1,
						pb_user_id		= '$pb_user_id'
					WHERE
						pb_kode			= '$pb_kode'
				")or die(mysql_error());
				
				//cari saldo keuangan terakhir
				$result2 = mysql_query("
					SELECT 
						keu_saldo
					FROM 
						keuangan 
					ORDER BY keu_id DESC
					LIMIT 1 
				")or die(mysql_error());
				if(mysql_num_rows($result2)){
					extract(mysql_fetch_assoc($result2));	
				}
				
				$saldo = $keu_saldo-$pb_sisa_bayar;
			
				//simpan ke rekening keuangan
				mysql_query("
					INSERT IGNORE INTO 
						keuangan 
					SET
						keu_tanggal			= NOW(),
						keu_kode			= '$pb_kode',
						keu_transaksi		= 'Pembelian',
						keu_mutasi_debet	= '$pb_sisa_bayar',
						keu_saldo			= '$saldo',
						keu_keterangan		= 'Pengurangan (sisa bayar)',
						keu_user_id			= '$pb_user_id'
				")or die(mysql_error());
				
				//cari saldo keuangan terakhir company
				$result3 = mysql_query("
					SELECT 
						c_keuangan
					FROM 
						company
					WHERE
						c_id =1
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				$keuangan = $c_keuangan-$pb_sisa_bayar;
				
				//Update ke saldo keuangan company
				mysql_query("
					UPDATE  
						company
					SET
						c_keuangan	= '$keuangan'
					WHERE
						c_id		= 1
				")or die(mysql_error());
		break;
		
		case 'save':
				$r = mysql_query("
					SELECT  *
					FROM
						pembelian
					WHERE
						pb_kode = '$pb_kode'
				")or die(mysql_error());
				if(mysql_num_rows($r)){
									
					//Update ke pembelian
					if($pb_bayar>0){
						mysql_query("
							UPDATE  
								pembelian 
							SET
								pb_tanggal		= NOW(),
								pb_suplier		= '$pb_suplier',
								pb_nota			= '$pb_nota',
								pb_jumlah		= '$pb_jumlah',
								pb_persen_diskon= '$pb_persendiskon',
								pb_diskon		= '$pb_diskon',
								pb_persen_ppn	= '$pb_persenppn',
								pb_ppn			= '$pb_ppn',
								pb_total		= '$pb_total',
								pb_bayar		= '$pb_bayar',
								pb_sisa_bayar	= '$pb_sisa_bayar',
								pb_status		= 1,
								pb_user_id		= '$pb_user_id'
							WHERE
								pb_kode			= '$pb_kode'
						")or die(mysql_error());	
					} else {
						mysql_query("
							UPDATE  
								pembelian 
							SET
								pb_tanggal		= NOW(),
								pb_suplier		= '$pb_suplier',
								pb_nota			= '$pb_nota',
								pb_jumlah		= '$pb_jumlah',
								pb_persen_diskon= '$pb_persendiskon',
								pb_diskon		= '$pb_diskon',
								pb_persen_ppn	= '$pb_persenppn',
								pb_ppn			= '$pb_ppn',
								pb_total		= '$pb_total',
								pb_bayar		= '$pb_bayar',
								pb_sisa_bayar	= '$pb_sisa_bayar',
								pb_user_id		= '$pb_user_id'
							WHERE
								pb_kode			= '$pb_kode'
						")or die(mysql_error());
					}
					
					//cari saldo keuangan terakhir
					$result2 = mysql_query("
						SELECT 
							keu_saldo
						FROM 
							keuangan 
						ORDER BY keu_id DESC
						LIMIT 1 
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					if($pb_bayar===$pb_total && $pb_sisa_bayar == 0) {
						$saldo = $keu_saldo-$pb_total;
					
						//simpan ke rekening keuangan
						mysql_query("
							INSERT INTO 
								keuangan 
							SET
								keu_tanggal			= NOW(),
								keu_kode			= '$pb_kode',
								keu_transaksi		= 'Pembelian',
								keu_mutasi_debet	= '$pb_total',
								keu_saldo			= '$saldo',
								keu_keterangan		= 'Pengurangan',
								keu_user_id			= '$pb_user_id'
						")or die(mysql_error());
						
						//cari saldo keuangan terakhir company
						$result3 = mysql_query("
							SELECT 
								c_keuangan
							FROM 
								company
							WHERE
								c_id =1
						")or die(mysql_error());
						if(mysql_num_rows($result3)){
							extract(mysql_fetch_assoc($result3));	
						}
						
						$keuangan = $c_keuangan-$pb_total;
						
						//Update ke saldo keuangan company
						mysql_query("
							UPDATE  
								company
							SET
								c_keuangan	= '$keuangan'
							WHERE
								c_id		= 1
						")or die(mysql_error());
					} else {
						$saldo = $keu_saldo-$pb_bayar;
					
						//simpan ke rekening keuangan
						mysql_query("
							INSERT INTO 
								keuangan 
							SET
								keu_tanggal			= NOW(),
								keu_kode			= '$pb_kode',
								keu_transaksi		= 'Pembelian',
								keu_mutasi_debet	= '$pb_bayar',
								keu_saldo			= '$saldo',
								keu_keterangan		= 'Pengurangan',
								keu_user_id			= '$pb_user_id'
						")or die(mysql_error());
						
						//cari saldo keuangan terakhir company
						$result3 = mysql_query("
							SELECT 
								c_keuangan
							FROM 
								company
							WHERE
								c_id =1
						")or die(mysql_error());
						if(mysql_num_rows($result3)){
							extract(mysql_fetch_assoc($result3));	
						}
						
						$keuangan = $c_keuangan-$pb_bayar;
						
						//Update ke saldo keuangan company
						mysql_query("
							UPDATE  
								company
							SET
								c_keuangan	= '$keuangan'
							WHERE
								c_id		= 1
						")or die(mysql_error());
					}
				}
		break;
		
		case 'delete': // fungsi belum ada tapi sudah disiapkan.. tinggal edit keuangan bila pembelian dihapus
			$r = mysql_query("
				SELECT  *
				FROM
					pembelian
				WHERE
					pb_kode = '$pb_kode'
			")or die(mysql_error());
			if(mysql_num_rows($r)){
				//hapus pembelian dahulu
				mysql_query("
					DELETE FROM 
						pembelian 
					WHERE
						pb_kode			= '$pb_kode'
				")or die(mysql_error());
				
				// hapus pembelian detail kemudian
				mysql_query("DELETE FROM pembelian_detail WHERE pbd_pb_kode='$pb_kode'")or die(mysql_error());
			}
		break;
		
		case 'save-item':
			$r = mysql_query("
				SELECT  *
				FROM
					pembelian
				WHERE
					pb_kode = '$pb_kode'
			")or die(mysql_error());
			if(!mysql_num_rows($r)){
				//proses pembelian detail
				if(!$pbd_id || $pbd_id == '' || $pbd_id==0) {	
				
					$stok = $p_stok+$pbd_beli; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok',
							p_tgl_masuk		= '$pbd_p_tgl_masuk',
							p_tgl_expire	= '$pbd_p_tgl_expire'
						WHERE 
							p_kode			='$pbd_p_kode'
					")or die(mysql_error());
					
					//simpan ke pembelian detail
					mysql_query("
						INSERT INTO 
							pembelian_detail 
						SET
							pbd_pb_kode			= '$pb_kode',
							pbd_p_k_id			= '$pbd_p_k_id',
							pbd_p_kode			= '$pbd_p_kode',
							pbd_p_nama			= '$pbd_p_nama',
							pbd_p_harga_beli	= '$pbd_p_harga_beli',
							pbd_p_satuan		= '$pbd_p_satuan',
							pbd_p_tgl_masuk		= '$pbd_p_tgl_masuk',
							pbd_p_tgl_expire	= '$pbd_p_tgl_expire',
							pbd_beli			= '$pbd_beli',
							pbd_jumlah			= '$pbd_jumlah'
					")or die(mysql_error());
				} 
				
				//cari jumlah total pembelian detail
				$result3 = mysql_query("
					SELECT 
						sum(pbd_jumlah) as jtotal
					FROM 
						pembelian_detail 
					WHERE pbd_pb_kode = '$pb_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				//simpan ke pembelian
				mysql_query("
					INSERT INTO 
						pembelian 
					SET
						pb_kode			= '$pb_kode',
						pb_tanggal		= NOW(),
						pb_suplier		= '$pb_suplier',
						pb_nota			= '$pb_nota',
						pb_jumlah		= '$jtotal',
						pb_total		= '$jtotal',
						pb_sisa_bayar	= '$jtotal',
						pb_user_id		= '$pb_user_id'
				")or die(mysql_error());
			} else { //kalau save-item kode beli sudah ada
				// proses pembelian detail
				if(!$pbd_id || $pbd_id == '' || $pbd_id==0) {
				
					$stok = $p_stok+$pbd_beli; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok',
							p_tgl_masuk		= '$pbd_p_tgl_masuk',
							p_tgl_expire	= '$pbd_p_tgl_expire'
						WHERE 
							p_kode			= '$pbd_p_kode'
					")or die(mysql_error()); 
					
					//simpan ke pembelian detail
					mysql_query("
						INSERT INTO 
							pembelian_detail 
						SET
							pbd_pb_kode			= '$pb_kode',
							pbd_p_k_id			= '$pbd_p_k_id',
							pbd_p_kode			= '$pbd_p_kode',
							pbd_p_nama			= '$pbd_p_nama',
							pbd_p_harga_beli	= '$pbd_p_harga_beli',
							pbd_p_satuan		= '$pbd_p_satuan',
							pbd_p_tgl_masuk		= '$pbd_p_tgl_masuk',
							pbd_p_tgl_expire	= '$pbd_p_tgl_expire',
							pbd_beli			= '$pbd_beli',
							pbd_jumlah			= '$pbd_jumlah'
					")or die(mysql_error());
				} else {					
					//cari jumlah beli sebelumnya
					$result2 = mysql_query("
						SELECT 
							pbd_beli  as beli_awal 
						FROM 
							pembelian_detail 
						WHERE pbd_id = '$pbd_id'
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					$stok_awal = $p_stok-$beli_awal; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			='$stok_awal',
							p_tgl_masuk		= '$pbd_p_tgl_masuk',
							p_tgl_expire	= '$pbd_p_tgl_expire'
						WHERE 
							p_kode			= '$pbd_p_kode'
					")or die(mysql_error());

					$stok = $stok_awal+$pbd_beli;
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			='$stok',
							p_tgl_masuk		= '$pbd_p_tgl_masuk',
							p_tgl_expire	= '$pbd_p_tgl_expire'
						WHERE 
							p_kode			= '$pbd_p_kode'
					")or die(mysql_error());
					
					//update ke pembelian detail
					mysql_query("
						UPDATE  
							pembelian_detail 
						SET
							pbd_pb_kode			= '$pb_kode',
							pbd_p_k_id			= '$pbd_p_k_id',
							pbd_p_kode			= '$pbd_p_kode',
							pbd_p_nama			= '$pbd_p_nama',
							pbd_p_harga_beli	= '$pbd_p_harga_beli',
							pbd_p_satuan		= '$pbd_p_satuan',
							pbd_p_tgl_masuk		= '$pbd_p_tgl_masuk',
							pbd_p_tgl_expire	= '$pbd_p_tgl_expire',
							pbd_beli			= '$pbd_beli',
							pbd_jumlah			= '$pbd_jumlah'
						WHERE
							pbd_id				= '$pbd_id'
					")or die(mysql_error());
				}
				
				//cari jumlah total pembelian detail 
				$result3 = mysql_query("
					SELECT 
						sum(pbd_jumlah) as jtotal 
					FROM 
						pembelian_detail 
					WHERE pbd_pb_kode = '$pb_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				//update ke pembelian
				mysql_query("
					UPDATE  
						pembelian 
					SET
						pb_tanggal		= NOW(),
						pb_suplier		= '$pb_suplier',
						pb_nota			= '$pb_nota',
						pb_jumlah		= '$jtotal',
						pb_total		= '$jtotal',
						pb_sisa_bayar	= '$jtotal',
						pb_user_id		= '$pb_user_id'
					WHERE
						pb_kode			= '$pb_kode'
				")or die(mysql_error());
			}
		break;
		
		case 'delete-item':						
			//cari total yang dikurangi/dihapus dan kode pembelian
			$result = mysql_query("
				SELECT 
					pbd_pb_kode,
					pbd_p_kode,
					pbd_beli as beli_awal,
					pbd_jumlah 
				FROM 
					pembelian_detail 
				WHERE pbd_id = '$pbd_id' 
			")or die(mysql_error());
			if(mysql_num_rows($result)){
				extract(mysql_fetch_assoc($result));	
			}
			
			//cari jumlah total dari pembelian
			$result2 = mysql_query("
				SELECT 
					pb_jumlah 
				FROM 
					pembelian 
				WHERE pb_kode = '$pbd_pb_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result2)){
				extract(mysql_fetch_assoc($result2));	
			}
			
			$sisajumlah = $pb_jumlah-$pbd_jumlah;
			$sisatotal	= $sisajumlah;
			
			//update ke pembelian
			mysql_query("
				UPDATE  
					pembelian 
				SET
					pb_tanggal		= NOW(),
					pb_jumlah		= '$sisajumlah',
					pb_total		= '$sisatotal',
					pb_sisa_bayar	= '$sisatotal',
					pb_user_id		= '$pb_user_id'
				WHERE
					pb_kode			= '$pbd_pb_kode'
			")or die(mysql_error());
			
			//cari stok produk terakhir
			$result3 = mysql_query("
				SELECT 
					p_stok as stok_akhir
				FROM 
					produk 
				WHERE p_kode = '$pbd_p_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result3)){
				extract(mysql_fetch_assoc($result3));	
			}
			
			$stok = $stok_akhir-$beli_awal; 
			
			// update stok produk
			mysql_query("
				UPDATE produk SET p_stok='$stok' WHERE p_kode='$pbd_p_kode'
			")or die(mysql_error());
			
			//hapus pembelian detail
			mysql_query("
				DELETE FROM pembelian_detail WHERE pbd_id='$pbd_id'
			")or die(mysql_error());		
		break;
	}
?>
