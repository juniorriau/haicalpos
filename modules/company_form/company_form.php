<?php
defined('_IEXEC')or die('');	

$title 		= 'Edit';
$query  = "
	SELECT  *
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
} else {
	$c_id 			= '';
	$c_nama			= '';
	$c_alamat		= '';
	$c_kontak		= '';
	$c_slogan		= '';
	$c_keuangan		= '';
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $c_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8">
			<div class="box box-info">
			<div class="box-body">	
				<div class="form-group">
					<input type="hidden" id="id" name="id" value="<?php echo $c_id; ?>"/>
					<label class="col-sm-3 control-label">Nama</label>
					<div class="col-sm-4">
						<input type="text" id="nama" name="nama" placeholder="Nama" class="form-control input-sm" required="true" value="<?php echo $c_nama;?>" autofocus/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Alamat</label>
					<div class="col-sm-6">
						<textarea class="form-control input-sm" id="alamat" name="alamat" rows="3" placeholder="Alamat Lengkap" required="true"><?php echo $c_alamat;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Kontak</label>
					<div class="col-sm-4">
						<input type="text" id="kontak" name="kontak" placeholder="Kontak (HP/Telpon/BBM/dll)" class="form-control input-sm" value="<?php echo $c_kontak;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Slogan</label>
					<div class="col-sm-6">
						<input type="text" id="slogan" name="slogan" placeholder="Slogan" class="form-control input-sm"value="<?php echo $c_slogan;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Saldo Keuangan</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="keuangan" name="keuangan" placeholder="0" class="form-control input-sm number-mask" required="true" value="<?php echo $c_keuangan;?>" />
						</div>
					</div>
				</div>    				
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div>
			</div>
			</div>
		</div>
	</form>
</div>
	
<script>
// Main Function		
$(function(){ 
	//initial
    set_mask();
	
	$("#btn-submit").click(function(){
		var	userid  	= <?php echo $userid;?>, 
			id	    	= $("#id").val(),
			nama   		= $("#nama").val(),
			alamat  	= $("#alamat").val(),
			kontak	    = $("#kontak").val(),
			slogan	   	= $("#slogan").val(),
			keuangan1	= <?php echo $c_keuangan?>,
			keuangan2  	= toNumber($("#keuangan").val()),
			query   	=   'type=save'+
							'&userid='+userid+
							'&id='+id+
							'&nama='+nama+
							'&alamat='+alamat+
							'&kontak='+kontak+
							'&slogan='+slogan+
							'&keuangan1='+keuangan1+
							'&keuangan2='+keuangan2;
		
		// validasi
		if (!nama) {
		   window.alert('Ooops!\nNama harus diisi');
		   $("#nama").focus();
		   return;
		}
		if(!alamat){
		   window.alert('Ooops!\nAlamat harus diisi');
		   $("#alamat").focus();
		   return;
		}
			
		$.ajax({
			url     : 'modules/company_form/company_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
               	window.alert("Data Berhasil Diperbaharui"); 
				window.location = '?p=<?php echo paramEncrypt('company_form');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>