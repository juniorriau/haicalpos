<?php 
defined('_IEXEC') or die('');

if(!isset($_REQUEST['kd'])){
	$title 			= 'New';
	//data penjualan
	$pj_kode			= gen_sid();
	$pj_tanggal			= '';
	$pj_jumlah			= '';
	$pj_persen_diskon	= '';
	$pj_diskon			= '';
	$pj_persen_ppn		= '';
	$pj_ppn				= '';
	$pj_total			= '';
	$pj_bayar			= '';
	$pj_kembali			= '';
	
	//data penjualan detail
	
	
}else{
	$title 		= 'Edit';
	$pj_kode	= paramDecrypt($_REQUEST['kd']);
	$query  = "
		SELECT  
			*
		FROM
			penjualan
		WHERE
			pj_kode = '$pj_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}

	$query  = "
		SELECT  
			*
		FROM
			company
		WHERE
			c_id = '1'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
	
	// Item Pembelian ==========================
	$q = mysql_query("
		 SELECT * FROM penjualan_detail WHERE pjd_pj_kode ='$pj_kode'
	")or die(mysql_error());
	$row_item='';
	$no =0;
	if(mysql_num_rows($q)){
		while($r=mysql_fetch_assoc($q)){
			extract($r);
			$no++;
			if(strlen($pjd_p_nama)>12){
				$pjd_p_nama_potong = substr($pjd_p_nama, 0, 12);
				$pjd_p_nama = $pjd_p_nama_potong.'..';
			}
			if(strlen($pjd_p_satuan)>3){
				$pjd_p_satuan = substr($pjd_p_satuan, 0, 3);
			}
			
			$row_item.='
				<tr>
					<td style="width:60%;">'.$no.'. '.ucwords($pjd_p_nama).' ('.$pjd_beli.' '.$pjd_p_satuan.')</td>
					<td style="width:40%;">'.rupiah($pjd_jumlah).'</td>
				</tr>
			';
		}
	}
//==============================================	
$file_name='PENJUALAN_'.$pj_kode.'.pdf';
ob_clean();
ob_start();
?>
<style>
	td{
	margin:0px;
	padding:0px;
	}
	
	p{
	margin:0px;
	padding:0px;
	}
</style>
<page style="font-size:10px;">
	<h3 style="width:100%; text-align:center; margin:0px; padding:0px;"><?php echo strtoupper($c_nama); ?></h3>
	<p style="width:100%; text-align:center;">
	<?php echo ucwords($c_slogan); ?>
	</p>
	<?php echo '-----------------------------------------------------------------';?>
	<p style="width:100%; text-align:center;">
	<?php echo ucwords($c_alamat); ?><br>
	<?php echo ucwords($c_kontak); ?>
	</p>
	<?php echo '-----------------------------------------------------------------';?>
	<p style="width:100%; text-align:center;">
	Kode : <?php echo $pj_kode;?>(<?php echo $pj_customer;?>)<br>Tanggal : <?php echo $pj_tanggal;?><br>Kasir : <?php echo get_fullname($pj_user_id);?>
	</p>
	<?php echo '-----------------------------------------------------------------';?>
	<table style="vertical-align:top; width:100%;">	
		<?php echo $row_item;?>
	</table>
	<?php echo '-----------------------------------------------------------------';?>
	<table style="vertical-align:top; width:100%;">
		<tr>
			<td style="width:20%;"></td>
			<td style="width:40%;">Total</td>
			<td style="width:40%;"><?php echo rupiah($pj_jumlah);?></td>
		</tr>
		<tr>
			<td style="width:20%;"></td>
			<td style="width:40%;">Diskon (<?php echo $pj_persen_diskon;?> %)</td>
			<td style="width:40%;"><?php echo rupiah($pj_diskon);?></td>
		</tr>
		<tr>
			<td style="width:20%;"></td>
			<td style="width:40%;">PPn (<?php echo $pj_persen_ppn;?> %)</td>
			<td style="width:40%;"><?php echo rupiah($pj_ppn);?></td>
		</tr>
		<tr>
			<td style="width:20%;"></td>
			<td style="width:40%;">Grand Total</td>
			<td style="width:40%;"><?php echo rupiah($pj_total);?></td>
		</tr>
		<tr>	
			<td style="width:20%;"></td>
			<td style="width:40%;">Bayar</td>
			<td style="width:40%;"><?php echo rupiah($pj_bayar);?></td>
		</tr>
		<tr>
			<td style="width:20%;"></td>
			<td style="width:40%;">Kembali</td>
			<td style="width:40%;"><?php echo rupiah($pj_kembali);?></td>
		</tr>
	</table>
	<p style="width:100%; text-align:center;">
	<?php echo '-----Terimakasih Atas Kunjungannya-----'; ?>
	<br>
	<?php echo 'printed: '; echo date('Y-m-d, h:i:s');?>
	</p>
</page>
<?php
	$content = ob_get_clean();

// convert in PDF
require_once('plugins/html2pdf/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P', array(58, 210), 'en', true, 'UTF-8', array(0, 0, 0, 0));//, true, 'UTF-8', array(10, 7, 30, 5));
	//$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(20, 5, 10, 5));//, true, 'UTF-8', array(10, 7, 30, 5));
	$html2pdf->pdf->SetMargins(0, 0, 0, 0);
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($file_name);
} 
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
?>