<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$month  = $_POST['m'];
	$year   = $_POST['y'];
	$show	= $_POST['show'];      // jumlah record per page
	$limit  = ($show)? ' LIMIT '.$show:''; // limit pada seleksi data
    
	$WhereMonth='';
	if(intval($month) > 0){
		$WhereMonth =" && MONTH(pj_tanggal) = ".$month." ";
	}

	$WhereYear='';
	if(intval($year) > 0){
		$WhereYear=" YEAR(pj_tanggal) = ".$year." ";
	}
	
	$query="
		SELECT  
			*
		FROM    
			penjualan 
		WHERE
		".$WhereYear.$WhereMonth."
		ORDER BY 
			pj_kode 
		ASC ". $limit;
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="10" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
					<td><b>'.$pj_customer.'</b></td>
                    <td><b>'.$pj_kode.'</b></td>
                    <td>'.showdt($pj_tanggal, 2).'</td>
                    <td>'.rupiah($pj_jumlah).'</td>
					<td>'.rupiah($pj_diskon).' '.persen($pj_persen_diskon).'</td>
                    <td>'.rupiah($pj_ppn).' '.persen($pj_persen_ppn).'</td>
					<td>'.rupiah($pj_total).'</td>
					<td>'.get_fullname($pj_user_id).'</td>
            '; 
			
			//cari retur penjualan sudah ada atau belum
			$result2 = mysql_query("
				SELECT 
					*
				FROM 
					retur_penjualan 
				WHERE rpj_pj_kode = '$pj_kode' 
			")or die(mysql_error());
			if(mysql_num_rows($result2)){
				extract(mysql_fetch_assoc($result2));
				
				if($rpj_total==0) {
					$content.= '
						<td>
							<a data-toggle="tooltip" title="Edit Retur Penjualan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('retur_penjualan_form').'&kdpj='.paramEncrypt($pj_kode).'&kdrpj='.paramEncrypt($rpj_kode).'&pjdid='.paramEncrypt(0).'" role="button"><i class="fa fa-edit"></i></a>
							<a data-toggle="tooltip" title="Export Struk Penjualan (PDF)" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('struk_penjualan_pdf').'&kd='.paramEncrypt($pj_kode).'" role="button"><i class="fa fa-download"></i></a>
						</td>
					</tr>
				';
				} else {
					$content.= '
						<td>
							<a data-toggle="tooltip" title="Export Struk Penjualan (PDF)" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('struk_penjualan_pdf').'&kd='.paramEncrypt($pj_kode).'" role="button"><i class="fa fa-download"></i></a>
						</td>
					</tr>
				';
				}	
			} else {				
				$content.= '
					<td>
						<a data-toggle="tooltip" title="Retur Penjualan" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('retur_penjualan_form').'&kdpj='.paramEncrypt($pj_kode).'" role="button"><i class="fa fa-hand-lizard-o"></i></a>
						<a data-toggle="tooltip" title="Export Struk Penjualan (PDF)" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('struk_penjualan_pdf').'&kd='.paramEncrypt($pj_kode).'" role="button"><i class="fa fa-download"></i></a>
					</td>
                </tr>
			';
			}
        }
    }
    echo $content;
?>