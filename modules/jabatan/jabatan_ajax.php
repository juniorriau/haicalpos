<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------

    $query="
     SELECT  
		*
     FROM    
		jabatan 
	 WHERE
		j_id != 1
     ORDER BY 
		j_id 
	 ASC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
			$status_color = ($j_aktif == 'Y')? 'text-green' : 'text-red';
            $j_aktif = ($j_aktif == 'Y')? 'Ya' : 'Tidak';
			$content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.ucwords($j_nama).'</b></td>
					<td>'.list_menu_akses($j_id).'</td>
                    <td class="'.$status_color.'">'.strtoupper($j_aktif).'</td>
                    <td>
						<a data-toggle="tooltip" title="Edit Jabatan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('jabatan_form').'&id='.paramEncrypt($j_id).'" role="button"><i class="fa fa-edit"></i></a>
						<a data-toggle="tooltip" title="Delete Jabatan" class="btn btn-xs btn-danger" href="javascript:del('.$j_id.')" role="button"><i class="fa fa-trash-o"></i></a>
					</td>
                </tr>
            ';
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/jabatan_form/jabatan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('jabatan');?>';
			}
		});
	}
}
</script>