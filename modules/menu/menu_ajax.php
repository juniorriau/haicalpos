<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
 
    $query="
     SELECT  
		*
     FROM    
		module_menu 
     ORDER BY 
		mm_id 
	 ASC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
			$status_color = ($mm_aktif == 'Y')? 'text-green' : 'text-red';
            $mm_aktif = ($mm_aktif == 'Y')? 'Ya' : 'Tidak';
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.ucwords($mm_nama).'</b></td>
                    <td>'.$mm_ikon.'/'.$mm_warna_ikon.'</td>
                    <td class="'.$status_color.'">'.strtoupper($mm_aktif).'</td>
                    <td>
						<a data-toggle = "tooltip" title = "Edit Menu" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('menu_form').'&id='.paramEncrypt($mm_id).'" role="button"><i class="fa fa-edit"></i></a>
						<a data-toggle = "tooltip" title = "Delete Menu" class="btn btn-xs btn-danger" href="javascript:del('.$mm_id.')" role="button"><i class="fa fa-trash-o"></i></a>
					</td>
                </tr>
            ';
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/menu_form/menu_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('menu');?>';
			}
		});
	}
}
</script>