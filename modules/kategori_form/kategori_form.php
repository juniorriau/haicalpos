<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$id 			= '';
	$k_nama		= '';
	$k_aktif		= '';
}else{
	$title 		= 'Edit';
	$id 		=  paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			kategori
		WHERE
			k_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $k_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-4">
			<div class="box box-info">
			<div class="box-body">
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama</label>
					<div class="col-sm-6">
						<input type="text" id="nama" name="nama" placeholder="Nama kategori" class="form-control input-sm" required="true" value="<?php echo $k_nama;?>" autofocus />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Aktif</label>
					<div class="col-sm-6">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($k_aktif);?>
						</select>	
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div>
			</div>
			</div>
			</div>
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('kategori');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End Box-->


<script>
// Main Function		
$(function(){ 
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			nama    	= $("#nama").val(),
			aktif		= $("#aktif").val();
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nNama kategori harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}
			
		var query   =   'type=save'+
        				'&id='+id+
						'&nama='+nama+
						'&aktif='+aktif;
		$.ajax({
			url     : 'modules/kategori_form/kategori_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}      
				window.location = '?p=<?php echo paramEncrypt('kategori');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>