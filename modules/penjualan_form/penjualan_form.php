<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['kd']) && !isset($_REQUEST['pjdid'])){
	$title 			= 'New';
	
	//data penjualan
	$pj_kode			= gen_pjid();
	$pj_customer		= 'Umum';
	$pj_customer_umum	= 1;
	$check_customer  	= $pj_customer_umum? 'checked' : '';
	$pj_tanggal			= '';
	$pj_jumlah			= '';
	$pj_persen_diskon	= 0;
	$pj_diskon			= '';
	$pj_persen_ppn		= 0;
	$pj_ppn				= '';
	$pj_total			= '';
	$pj_bayar			= '';
	$pj_kembali			= '';
	
	$cari			= '';
	
	//data item penjualan detail
	$idp			= '';
	$kategori		= '';
	$kodep			= '';
	$namap			= '';
	$harga_jual		= '';
	$harga_beli		= '';
	$satuan			= '';
	$beli			= '';
	$jumlah			= '';
	
	$stok			= '';
	
}else{
	$title 		= 'Edit';
	$pj_kode	= paramDecrypt($_REQUEST['kd']);
	$pjd_id		= paramDecrypt($_REQUEST['pjdid']);
	
	//data penjualan
	$query  = "
		SELECT  
			*
		FROM
			penjualan
		WHERE
			pj_kode = '$pj_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));		
	}
	
	$check_customer  	= $pj_customer_umum? 'checked' : '';
	
	//data item penjualan detail
	if($pjd_id > 0){
		$query3  = "
			SELECT  
				pjd_id as idp, 
				pjd_p_k_id as kategori,
				pjd_p_kode as kodep, 
				pjd_p_nama as namap, 
				pjd_p_harga_jual as harga_jual,
				pjd_p_harga_beli as harga_beli,
				pjd_p_satuan as satuan,
				pjd_beli as beli,
				pjd_jumlah as jumlah,
				p_stok as stok
			FROM
				penjualan_detail
			INNER JOIN
				produk
			ON
				pjd_p_kode = p_kode
			WHERE
				pjd_id = '$pjd_id'
		";
		$result3 = mysql_query($query3)or die(mysql_error());
		if(mysql_num_rows($result3)){
			extract(mysql_fetch_assoc($result3));
			$cari = $kodep.' - '.ucwords($namap).' - '.rupiah($harga_jual).' / '.$satuan;
		}
	} else {
		$cari			= '';
		$idp			= '';
		$kategori		= '';
		$kodep			= '';
		$namap			= '';
		$harga_jual		= '';
		$harga_beli		= '';
		$satuan			= '';
		$beli			= '';
		$jumlah			= '';
		
		$stok			= '';
	}	
}
	// loading data untuk auto complete
    $q = mysql_query("
        SELECT 
			*
        FROM 
			produk
        WHERE 
			p_aktif = 'Y'             
        ORDER BY
			p_id ASC         
    ")or die(mysql_error());
    
    $data       = '[';
    $num_data   = mysql_num_rows($q);
    $no         = 0;
    while($r = mysql_fetch_array($q)){
        $no++;
        extract($r);

        $data .= '
            {
                "value":"'.$p_kode.' - '.ucwords($p_nama).' - '.rupiah($p_harga_jual).' / '.$p_satuan.'",
                "kode":"'.$p_kode.'",
                "nama":"'.ucwords($p_nama).'",
                "hargajual":"'.$p_harga_jual.'",
				"hargabeli":"'.$p_harga_beli.'",
				"satuan":"'.$p_satuan.'",
				"stok":"'.$p_stok.'",
				"kategori":"'.$p_k_id.'"
            }    
        ';
               
        if($no < $num_data){
            $data .=',';    
        }
    }
    $data .=']';
?>

<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $pj_kode;?></h3>
	</div><!-- /.box-header utama -->
	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8"><!-- div col md 8 data penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Data Penjualan</h3>
			</div><!-- /.box-header data penjualan -->
			<div class="box-body">	
				<div class="form-group">
					<label class="col-sm-3 control-label">Kode Penjualan</label>
					<div class="col-sm-4">
						<input type="text" id="kodejual" name="kodejual" class="form-control input-sm" value="<?php echo $pj_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Customer</label>
					<div class="col-sm-4">
						<input type="text" id="customer" name="customer" class="form-control input-sm" placeholder="Nama Customer" value="<?php echo $pj_customer;?>" />
					</div>
					<div class="col-sm-2">
						<input type="checkbox" id="customer_check" name="customer_check" <?php echo $check_customer;?>> Umum
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Cari Product</label>
					<div class="col-sm-8">
						<input type="text" id="cari" name="cari" class="form-control input-sm" onclick="clearInput(this)" placeholder="Nama Product" value="<?php echo $cari;?>" autofocus />
					</div>
				</div>
			</div><!-- end div box-body data penjualan-->
			</div><!-- end div box data penjualan -->
			</div><!-- end div col md 8 data penjualan -->
			
			<div class="col-md-12"><!-- div col md 12 item penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Item Penjualan Product</h3>
			</div><!-- /.box-header item penjualan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" style="margin-bottom:2px;">
							<thead>
								<tr class="success">
								<th></th>
								<th></th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th></th>
								<th></th>
								<th>Beli Product</th>
								<th>Jumlah Harga</th>
								<th></th>
								</tr>
								<tr>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="pjd_id" name="pjd_id" value="<?php echo $idp;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="kategori" name="kategori" value="<?php echo $kategori;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="kodeprod" name="kodeprod" value="<?php echo $kodep;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="nama" name="nama" value="<?php echo $namap;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="hargajual" name="hargajual" value="<?php echo $harga_jual;?>" readonly /><span class="input-group-addon">/</span><span class="input-group-addon" id="satuan" name="satuan"><?php echo $satuan;?></span></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="hargabeli" name="hargabeli" value="<?php echo $harga_beli;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="stok" name="stok" value="<?php echo $stok;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="text" id="beli" name="beli" placeholder="0" value="<?php echo $beli;?>" /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="jumlah" name="jumlah" value="<?php echo $jumlah;?>" readonly /></div></th>
								<th>
									<?php 
									if($idp==0 || !$idp){
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Add</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-del" name="btn-del">Remove</button>
										';
									} else {
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Update</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-can" name="btn-can">Cancel</button>
										';
									}
									?>
								</th>     
								<tr>
							</thead>
							<tbody id="item-penjualan">
							</tbody>
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body item penjualan -->
			</div><!-- end div box item penjualan -->
			</div><!-- end div col md 12 item penjualan -->
			
			<div class="col-md-12"><!-- div col md 12 daftar penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Daftar Penjualan Product</h3>
			</div><!-- /.box-header item penjualan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" style="margin-bottom:2px;">
								<tr class="success">
								<th>No</th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th>Beli Product</th>
								<th>Jumlah</th>
								<th>...</th>
								</tr>
							<tbody>
								<?php
									//data penjualan detail
									$query2  = "
										SELECT  
											*
										FROM
											penjualan_detail
										WHERE
											pjd_pj_kode = '$pj_kode'
									";
									$result2 = mysql_query($query2)or die(mysql_error());
	
									if(mysql_num_rows($result2)>0){
										$no = 0;
										while($r = mysql_fetch_array($result2)){
											extract($r);
											$no++;
											echo '
												<tr>
													<td>'.$no.'</td>
													<td>'.$pjd_p_kode.'</td>
													<td>'.$pjd_p_nama.'</td>
													<td>'.rupiah($pjd_p_harga_jual).' / '.$pjd_p_satuan.'</td>
													<td>'.$pjd_beli.'</td>
													<td>'.rupiah($pjd_jumlah).'</td>
													<td>
														<a data-toggle="tooltip" title="Edit Item Penjualan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('penjualan_form').'&kd='.paramEncrypt($pj_kode).'&pjdid='.paramEncrypt($pjd_id).'"><i class="fa fa-edit"></i></a>
														<a data-toggle="tooltip" title="Delete Item Penjualan" class="btn btn-xs btn-danger" href="javascript:ItemDel('.$pjd_id.')" role="button"><i class="fa fa-trash-o"></i></a>
													</td>
												</tr>
											';
										}
									} else {
										echo '<tr><td colspan="7" class="text-center">Data Kosong</td></tr>';
									}									
								?>
							</tbody>							
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body daftar penjualan -->
			</div><!-- end div box daftar penjualan -->
			</div><!-- end div col md 12 daftar penjualan -->
			
			<div class="col-md-6"><!-- div col md 4 detail pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Detail Pembayaran</h3>
			</div><!-- /.box-header detail pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Jumlah Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jtotal" name="jtotal"  class="form-control input-sm number-mask" value="<?php echo $pj_jumlah;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Diskon</label>
					<div class="col-sm-3">
						<div class="input-group input-group-sm">
							<select class="form-control input-sm" id="persendiskon" name="persendiskon" required="true">	
							<?php opt_diskon($pj_persen_diskon);?>
							</select><span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="diskon" name="diskon" class="form-control input-sm number-mask" value="<?php echo $pj_diskon;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">PPn</label>
					<div class="col-sm-3">
						<div class="input-group input-group-sm">
							<select class="form-control input-sm" id="persenppn" name="persenppn" required="true">	
							<?php opt_ppn($pj_persen_ppn);?>
							</select><span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="ppn" name="ppn" class="form-control input-sm number-mask" value="<?php echo $pj_ppn;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Grand Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="gtotal" name="gtotal" class="form-control input-sm number-mask" value="<?php echo $pj_total;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body detail pembayaran -->
			</div><!-- end div box detail pembayaran -->
			</div><!-- end div col md 6 detail pembayaran -->
			
			<div class="col-md-4"><!-- div col md 4 pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Pembayaran</h3>
			</div><!-- /.box-header detail pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Bayar</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">	
							<span class="input-group-addon">Rp</span>
							<input type="text" id="bayar" name="bayar" class="form-control input-sm number-mask" value="<?php echo $pj_bayar;?>" placeholder="0"/>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Kembali</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="kembali" name="kembali" class="form-control input-sm number-mask" value="<?php echo $pj_kembali;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body pembayaran -->
			</div><!-- end div box pembayaran -->
			</div><!-- end div col md 4 pembayaran -->
			
			<div class="panel-footer">
				<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
			</div> 
			
		</div><!-- end div body utama -->
	</form><!-- end form-->
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('penjualan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div> <!-- end div box-footer utama -->
</div> <!-- End box utama-->

<script>
//fungsi hapus item penjualan
function ItemDel(id) {
	var id		= id,
		query	= 'type=delete-item'+
				  '&pjdid='+id;
	var pilih	= confirm('Yakin akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/penjualan_form/penjualan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	//window.alert("Data Berhasil Dihapus");      
				location.reload();
			}
		});
	}
}


// Main Function		
$(function(){ 	
	var	userid  	= <?php echo $userid;?>,
		hasil		= <?php echo $data;?>;
	//initial
    set_mask();
	
	if(<?php echo $pj_customer_umum;?>){
       $("#customer").prop('readonly',true);
    }else{
       $("#customer").prop('readonly',false);
    }
	
	// checkbox customer umum change
    $("#customer_check").change(function(){
		if($(this).is(':checked')){
			$("#customer").val('Umum');
			$("#customer").prop('readonly',true);
			$("#cari").focus();
		}else{
			$("#customer").prop('readonly',false);
			$("#customer").focus();
		}
    })
	
	
	$('#cari').autocomplete({
		lookup: hasil,
		onSelect: function (suggestion) {			
			if(suggestion.stok==0) {
				alert('Stok kosong! \nSilahkan cari produk yang lain');	
				$("#kategori, #kodeprod, #nama, #hargajual, #stok, #beli, #jumlah, #cari").val('');
				$("#cari").focus();
			} else {
				$("#kategori").val(suggestion.kategori);
				$("#kodeprod").val(suggestion.kode);
				$("#nama").val(suggestion.nama);
				$("#hargajual").val(suggestion.hargajual);
				$("#hargabeli").val(suggestion.hargabeli);
				$("#stok").val(suggestion.stok);
				$("#beli").val(1);
				$("#satuan").text(suggestion.satuan);
			
				var hargajual	= toNumber($("#hargajual").val()),
					beli   		= toNumber($("#beli").val()),
					jumlah 		= hargajual*(beli/1);
					
				$("#jumlah").val(jumlah);
				$("#beli").focus();
			}
			
			
		}
	});
		
	$("#beli").keyup(function(){
		var hargajual	= toNumber($("#hargajual").val()),
			beli   		= toNumber($("#beli").val()),
			stok   		= toNumber($("#stok").val()),
			jumlah		= hargajual*(beli/1);
			
		if(beli>stok){
			window.alert('Ooops!\nStok tidak cukup! \nSesuaikan jumlah beli dengan stok');
			$("#beli").val(1);
			$("#beli").focus();
			exit;
		}
		
		$("#jumlah").val(jumlah);
	});
	
	$("#btn-del").click(function(e){
        e.preventDefault();
		$("#kategori, #kodeprod, #nama, #hargajual, #stok, #beli, #jumlah, #cari").val('');
		$("#cari").focus();
    })
	
	$("#btn-can").click(function(e){
        e.preventDefault();
		window.location = '?p=<?php echo paramEncrypt('penjualan_form');?>&kd=<?php echo paramEncrypt($pj_kode);?>&pjdid=<?php echo paramEncrypt(0);?>';
    })
	
	$("#btn-add").click(function(e){
		e.preventDefault();
		
		var kodejual		= $("#kodejual").val(),
			customer		= $("#customer").val(),
			pjdid			= $("#pjd_id").val(),
			kategori		= $("#kategori").val(),
			kodeprod		= $("#kodeprod").val(),
			nama   			= $("#nama").val(),
			hargajual		= toNumber($("#hargajual").val()),
			hargabeli		= toNumber($("#hargabeli").val()),
			satuan			= $("#satuan").text(),
			stok   			= toNumber($("#stok").val()),
			beli   			= toNumber($("#beli").val()),
			jumlah 			= toNumber($("#jumlah").val()),
			
			query			= 'userid='+userid+
							'&type=save-item'+
							'&kodejual='+kodejual+
							'&customer='+customer+
							'&customerumum='+get_CheckValue('customer_check')+
							'&pjdid='+pjdid+
							'&kategori='+kategori+
							'&kodeprod='+kodeprod+
							'&nama='+nama+
							'&hargajual='+hargajual+
							'&hargabeli='+hargabeli+
							'&satuan='+satuan+
							'&stok='+stok+
							'&beli='+beli+
							'&jumlah='+jumlah;
							
		// validate 
		if(!kategori && !kodeprod && !nama && !hargajual && !jumlah){
			window.alert('Ooops!\nItem Penjualan Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(!beli){
			window.alert('Ooops!\nBeli tidak boleh kosong!');
			$("#beli").focus();
			exit;
		}
		
		if(beli>stok){
			window.alert('Ooops!\nStok tidak cukup! \n Sesuaikan jumlah beli dengan stok');
			$("#beli").focus();
			exit;
		}
		
		if(beli==0 || !beli){
			window.alert('Ooops!\nBeli tidak boleh kosong Minimanl 1');
			$("#beli").focus();
			exit;
		}
		
		// save via ajax
        $.ajax({
			url     : 'modules/penjualan_form/penjualan_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				//window.alert("Data Berhasil Disimpan");		
				window.location = '?p=<?php echo paramEncrypt('penjualan_form');?>&kd=<?php echo paramEncrypt($pj_kode);?>&pjdid=<?php echo paramEncrypt(0);?>';					
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		}); 
	})
	
	$("#persendiskon").change(function(){
		var	jtotal			= toNumber($("#jtotal").val()),
			persendiskon	= toNumber($("#persendiskon").val()),
			diskon			= persendiskon/100*jtotal;
		
		$("#diskon").val(diskon);
		
		var	jtotal		= toNumber($("#jtotal").val()),
			diskon		= toNumber($("#diskon").val()),
			jtotal2		= jtotal-diskon,
			persenppn	= toNumber($("#persenppn").val()),
			ppn			= persenppn/100*jtotal2,
			gtotal		= jtotal2+ppn;
		
		$("#ppn").val(ppn);	
		$("#gtotal").val(gtotal);
		$("#bayar").focus();
    })
	
	$("#persenppn").change(function(){		
		var	jtotal		= toNumber($("#jtotal").val()),
			diskon		= toNumber($("#diskon").val()),
			jtotal2		= jtotal-diskon,
			persenppn	= toNumber($("#persenppn").val()),
			ppn			= persenppn/100*jtotal2,
			gtotal		= jtotal2+ppn;
		
		$("#ppn").val(ppn);	
		$("#gtotal").val(gtotal);
		$("#bayar").focus();
    })
	
	$("#bayar").keyup(function(){
		var	gtotal	= toNumber($("#gtotal").val()),
			bayar	= toNumber($("#bayar").val()),
			kembali	= bayar-gtotal;
		$("#kembali").val(kembali);
	})
	
	$("#btn-submit").click(function(e){
		e.preventDefault();
		
        var kodejual		= $("#kodejual").val(),
			customer		= $("#customer").val(),
			jtotal			= toNumber($("#jtotal").val()),
			persendiskon	= toNumber($("#persendiskon").val()),
			diskon			= toNumber($("#diskon").val()),
			persenppn		= toNumber($("#persenppn").val()),
			ppn				= toNumber($("#ppn").val()),
			gtotal			= toNumber($("#gtotal").val()),
			bayar			= toNumber($("#bayar").val()),
			kembali			= toNumber($("#kembali").val());
			
            query		= 'userid='+userid+
						'&type=save'+
						'&kodejual='+kodejual+
						'&customer='+customer+
						'&customerumum='+get_CheckValue('customer_check')+
						'&jtotal='+jtotal+
						'&persendiskon='+persendiskon+
						'&diskon='+diskon+
						'&persenppn='+persenppn+
						'&ppn='+ppn+
						'&gtotal='+gtotal+
						'&bayar='+bayar+
						'&kembali='+kembali;

		// validate
		if(!customer){
			window.alert('Customer masih kosong');
			$("#customer").focus();
			exit;
		}
		
		// validate	
		if(!jtotal && !ppn && !gtotal){
			window.alert('Ooops!\nItem Penjualan Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(jtotal==0 && ppn==0 && gtotal==0){
			window.alert('Ooops!\nItem Penjualan Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(!bayar && !kembali){
			window.alert('Ooops!\nBayar Belum ada!');
			$("#bayar").focus();
			exit;
		}
		
		if(bayar==0){
			window.alert('Ooops!\nBayar Belum ada!');
			$("#bayar").focus();
			exit;
		}
		
		if(bayar<gtotal){
			window.alert('Ooops!\nBayar Kurang!');
			$("#bayar").focus();
			exit;
		}

        // save via ajax
        $.ajax({
            url     : 'modules/penjualan_form/penjualan_form_ajax.php',
            data    : query,
            cache   : false,
            type    : 'post',
            success : function(data) {
				//window.alert("Data Berhasil Disimpan");
				//window.location = '?p=<?php echo paramEncrypt('penjualan');?>';
				//window.open(url,'_blank');
				window.location = '?p=<?php echo paramEncrypt('struk_penjualan_pdf');?>&kd=<?php echo paramEncrypt($pj_kode);?>';
				//window.location = 'modules/struk_penjualan_print/struk_penjualan_print.php?kd=<?php echo paramEncrypt($pj_kode);?>';
            },
            error : function(xhr, textStatus, errorThrown) {
                alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
                return false; //exit();
            }
        });
    })
	
})// End Main Function

</script>