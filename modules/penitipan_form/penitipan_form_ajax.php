<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$type				= $_POST['type'];
	
	//data penitipan	
	$pn_kode			= $_POST['kodetitip'];
	$pn_suplier			= $_POST['suplier'];
	$pn_nota			= $_POST['nota'];
	$pn_total_bayar		= $_POST['jbayar'];
	$pn_user_id			= $_POST['userid'];
		
	//data penitipan detail
	$pnd_id				= $_POST['pndid'];
	$pnd_p_k_id			= $_POST['kategori'];
	$pnd_p_kode			= $_POST['kodeprod'];
	$pnd_p_nama			= $_POST['nama'];
	$pnd_p_harga_beli	= $_POST['hargabeli'];
	$pnd_p_satuan		= $_POST['satuan'];
	$pnd_p_tgl_masuk	= date("Y-m-d");
	$pnd_p_tgl_expire	= $_POST['tglexpire'];
	$pnd_titip			= $_POST['titip'];
	$pnd_jumlah			= $_POST['jumlah'];
	$pnd_terjual		= $_POST['terjual'];
	$pnd_bayar			= $_POST['bayar'];
	
	//data produk
	$p_stok				= $_POST['stok'];
	
	switch ($type) {		
		case 'save':
			if($pn_total_bayar>0){
				$res = mysql_query("
					SELECT pnd_p_kode
					FROM
						penitipan
					INNER JOIN
						penitipan_detail
					ON
						pn_kode = pnd_pn_kode
					WHERE
						pn_kode = '$pn_kode'
				")or die(mysql_error());
				if(mysql_num_rows($res)){
					while($r = mysql_fetch_array($res)){
						extract($r);
						
						$r2 = mysql_query("
							SELECT pnd_id, pnd_p_harga_beli, pnd_titip, p_stok
							FROM
								penitipan_detail
							INNER JOIN
								produk
							ON
								pnd_p_kode = p_kode
							WHERE
								pnd_p_kode = '$pnd_p_kode'
						")or die(mysql_error());
						if(mysql_num_rows($r2)){
							extract(mysql_fetch_assoc($r2));
						}
						
						if($pnd_titip>$p_stok){
							$terjual	= $pnd_titip-$p_stok;
						} else {
							$terjual	= 0;
						}
						$sisa		= $p_stok;
						$bayar		= $terjual*$pnd_p_harga_beli;
						
						//update ke penitipan detail
						mysql_query("
							UPDATE  
								penitipan_detail 
							SET
								pnd_terjual			= '$terjual',
								pnd_sisa			= '$sisa',
								pnd_bayar			= '$bayar'
							WHERE
								pnd_id				= '$pnd_id'
						")or die(mysql_error());
									
						// update stok produk
						mysql_query("
							UPDATE 
								produk 
							SET 
								p_stok			= 0
							WHERE 
								p_kode			='$pnd_p_kode'
						")or die(mysql_error());
						
						//cari saldo keuangan terakhir
						$result2 = mysql_query("
							SELECT 
								keu_saldo
							FROM 
								keuangan 
							ORDER BY keu_id DESC
							LIMIT 1 
						")or die(mysql_error());
						if(mysql_num_rows($result2)){
							extract(mysql_fetch_assoc($result2));	
						}
						
						$saldo = $keu_saldo-$bayar;
						
						if($bayar>0) {
							//simpan ke rekening keuangan
							mysql_query("
								INSERT INTO 
									keuangan 
								SET
									keu_tanggal			= NOW(),
									keu_kode			= '$pn_kode',
									keu_transaksi		= 'Penitipan',
									keu_mutasi_debet	= '$bayar',
									keu_saldo			= '$saldo',
									keu_keterangan		= 'Pengurangan',
									keu_user_id			= '$pn_user_id'
							")or die(mysql_error());
						}
						
						//cari saldo keuangan terakhir company
						$result3 = mysql_query("
							SELECT 
								c_keuangan
							FROM 
								company
							WHERE
								c_id =1
						")or die(mysql_error());
						if(mysql_num_rows($result3)){
							extract(mysql_fetch_assoc($result3));	
						}
						
						$keuangan = $c_keuangan-$bayar;
						if($bayar>0) {
							//Update ke saldo keuangan company
							mysql_query("
								UPDATE  
									company
								SET
									c_keuangan	= '$keuangan'
								WHERE
									c_id		= 1
							")or die(mysql_error());
						}
					}
					
					//Update ke penitipan
					mysql_query("
						UPDATE  
							penitipan 
						SET
							pn_tanggal		= NOW(),
							pn_suplier		= '$pn_suplier',
							pn_nota			= '$pn_nota',
							pn_total_bayar	= '$pn_total_bayar',
							pn_status		= 1,
							pn_user_id		= '$pn_user_id'
						WHERE
							pn_kode			= '$pn_kode'
					")or die(mysql_error());	
				}
			}
		break;
		
		case 'delete': // fungsi belum ada tapi sudah disiapkan.. tinggal edit keuangan bila penitipan dihapus
			$r = mysql_query("
				SELECT  *
				FROM
					penitipan
				WHERE
					pn_kode = '$pn_kode'
			")or die(mysql_error());
			if(mysql_num_rows($r)){
				//hapus penitipan dahulu
				mysql_query("
					DELETE FROM 
						penitipan 
					WHERE
						pn_kode			= '$pn_kode'
				")or die(mysql_error());
				
				// hapus penitipan detail kemudian
				mysql_query("DELETE FROM penitipan_detail WHERE pnd_pn_kode='$pn_kode'")or die(mysql_error());
			}
		break;
		
		case 'save-item':
			$r = mysql_query("
				SELECT  *
				FROM
					penitipan
				WHERE
					pn_kode = '$pn_kode'
			")or die(mysql_error());
			if(!mysql_num_rows($r)){
				//proses penitipan detail
				if(!$pnd_id || $pnd_id == '' || $pnd_id==0) {	
				
					$stok = $p_stok+$pnd_titip; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok',
							p_tgl_masuk		= '$pnd_p_tgl_masuk',
							p_tgl_expire	= '$pnd_p_tgl_expire'
						WHERE 
							p_kode			='$pnd_p_kode'
					")or die(mysql_error());
					
					//simpan ke penitipan detail
					mysql_query("
						INSERT INTO 
							penitipan_detail 
						SET
							pnd_pn_kode			= '$pn_kode',
							pnd_p_k_id			= '$pnd_p_k_id',
							pnd_p_kode			= '$pnd_p_kode',
							pnd_p_nama			= '$pnd_p_nama',
							pnd_p_harga_beli	= '$pnd_p_harga_beli',
							pnd_p_satuan		= '$pnd_p_satuan',
							pnd_p_tgl_masuk		= '$pnd_p_tgl_masuk',
							pnd_p_tgl_expire	= '$pnd_p_tgl_expire',
							pnd_titip			= '$pnd_titip',
							pnd_jumlah			= '$pnd_jumlah'
					")or die(mysql_error());
				} 
								
				//simpan ke penitipan
				mysql_query("
					INSERT INTO 
						penitipan 
					SET
						pn_kode			= '$pn_kode',
						pn_tanggal		= NOW(),
						pn_suplier		= '$pn_suplier',
						pn_nota			= '$pn_nota',
						pn_user_id		= '$pn_user_id'
				")or die(mysql_error());
			} else { //kalau save-item kode titip sudah ada
				// proses penitipan detail
				if(!$pnd_id || $pnd_id == '' || $pnd_id==0) {
				
					$stok = $p_stok+$pnd_titip; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok',
							p_tgl_masuk		= '$pnd_p_tgl_masuk',
							p_tgl_expire	= '$pnd_p_tgl_expire'
						WHERE 
							p_kode			= '$pnd_p_kode'
					")or die(mysql_error()); 
					
					//simpan ke penitipan detail
					mysql_query("
						INSERT INTO 
							penitipan_detail 
						SET
							pnd_pn_kode			= '$pn_kode',
							pnd_p_k_id			= '$pnd_p_k_id',
							pnd_p_kode			= '$pnd_p_kode',
							pnd_p_nama			= '$pnd_p_nama',
							pnd_p_harga_beli	= '$pnd_p_harga_beli',
							pnd_p_satuan		= '$pnd_p_satuan',
							pnd_p_tgl_masuk		= '$pnd_p_tgl_masuk',
							pnd_p_tgl_expire	= '$pnd_p_tgl_expire',
							pnd_titip			= '$pnd_titip',
							pnd_jumlah			= '$pnd_jumlah'
					")or die(mysql_error());
				} else {					
					//cari jumlah titip sebelumnya
					$result2 = mysql_query("
						SELECT 
							pnd_titip  as titip_awal 
						FROM 
							penitipan_detail 
						WHERE pnd_id = '$pnd_id'
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					$stok_awal = $p_stok-$titip_awal; 
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok_awal',
							p_tgl_masuk		= '$pnd_p_tgl_masuk',
							p_tgl_expire	= '$pnd_p_tgl_expire'
						WHERE 
							p_kode			= '$pnd_p_kode'
					")or die(mysql_error());

					$stok = $stok_awal+$pnd_titip;
					
					// update stok produk
					mysql_query("
						UPDATE 
							produk 
						SET 
							p_stok			= '$stok',
							p_tgl_masuk		= '$pnd_p_tgl_masuk',
							p_tgl_expire	= '$pnd_p_tgl_expire'
						WHERE 
							p_kode			= '$pnd_p_kode'
					")or die(mysql_error());
					
					//update ke penitipan detail
					mysql_query("
						UPDATE  
							penitipan_detail 
						SET
							pnd_pn_kode			= '$pn_kode',
							pnd_p_k_id			= '$pnd_p_k_id',
							pnd_p_kode			= '$pnd_p_kode',
							pnd_p_nama			= '$pnd_p_nama',
							pnd_p_harga_beli	= '$pnd_p_harga_beli',
							pnd_p_satuan		= '$pnd_p_satuan',
							pnd_p_tgl_masuk		= '$pnd_p_tgl_masuk',
							pnd_p_tgl_expire	= '$pnd_p_tgl_expire',
							pnd_titip			= '$pnd_titip',
							pnd_jumlah			= '$pnd_jumlah'
						WHERE
							pnd_id				= '$pnd_id'
					")or die(mysql_error());
				}
				
				$kd = gen_pnid();
				//simpan ke penitipan
				mysql_query("
					UPDATE  
						penitipan 
					SET
						pn_tanggal		= NOW(),
						pn_suplier		= '$pn_suplier',
						pn_nota			= '$pn_nota',
						pn_user_id		= '$pn_user_id'
					WHERE
						pn_kode			= '$pn_kode'
				")or die(mysql_error());
			}
		break;
		
		case 'delete-item':								
			//update ke penitipan
			mysql_query("
				UPDATE  
					penitipan 
				SET
					pn_tanggal		= NOW(),
					pn_user_id		= '$pn_user_id'
				WHERE
					pn_kode			= '$pn_kode'
			")or die(mysql_error());
			
			//cari stok produk terakhir
			$result3 = mysql_query("
				SELECT 
					p_stok as stok_akhir
				FROM 
					produk 
				WHERE 
					p_kode = '$pnd_p_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result3)){
				extract(mysql_fetch_assoc($result3));	
			}
			
			$stok = $stok_akhir-$titip_awal; 
			
			// update stok produk
			mysql_query("
				UPDATE produk SET p_stok='$stok' WHERE p_kode='$pnd_p_kode'
			")or die(mysql_error());
			
			//hapus penitipan detail
			mysql_query("
				DELETE FROM penitipan_detail WHERE pnd_id='$pnd_id'
			")or die(mysql_error());		
		break;
	}
?>
