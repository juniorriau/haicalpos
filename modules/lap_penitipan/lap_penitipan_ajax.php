<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate			= $_POST['sdate'];
	$edate			= $_POST['edate'];
	$kategori_id	= $_POST['kategori'];
	
	if ($kategori_id<=0){
		$query="
		SELECT  
			pnd_p_k_id
		FROM    
			penitipan,		
			penitipan_detail 
		WHERE
			penitipan.pn_kode 	= penitipan_detail.pnd_pn_kode
			&& pn_tanggal	>= '$sdate'
			&& pn_tanggal	<= '$edate'
		GROUP BY 
			pnd_p_k_id
		";
		$result = mysql_query($query) or die(mysql_error());
	} else {
		$query="
		SELECT  
			pnd_p_k_id,
			pnd_p_kode,
            pnd_p_nama,
            pnd_p_harga_beli,
			pnd_p_satuan,
            SUM(pnd_titip) as titip,
            SUM(pnd_jumlah) as jumlah,
			SUM(pnd_terjual) as terjual,
			SUM(pnd_bayar) as bayar,
			SUM(pnd_sisa) as sisa,
			pn_status as status
		FROM    
			penitipan,		
			penitipan_detail 
		WHERE
			penitipan.pn_kode 	= penitipan_detail.pnd_pn_kode
			&& pn_tanggal	>= '$sdate'
			&& pn_tanggal	<= '$edate'
			&& pnd_p_k_id	= '$kategori_id'
		GROUP BY 
			pnd_p_kode
		";
		$result = mysql_query($query) or die(mysql_error());
	}
	$content = '';
    if(mysql_num_rows($result)>0){
		if ($kategori_id<=0) {
			$que="
			SELECT  
				SUM(pnd_bayar) as gbayar
			FROM    
				penitipan,
				penitipan_detail
			WHERE
				pn_tanggal		>= '$sdate'
				&& pn_tanggal	<= '$edate'
			";
			$resu = mysql_query($que) or die(mysql_error());
			$rw = mysql_fetch_assoc($resu);
			extract($rw);
			
			while($row = mysql_fetch_assoc($result)){
				extract($row);
				$content .= '
				<h5>Tanggal Pembelian '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($pnd_p_k_id).')</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr class="success">
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Titip</th>
						<th>Harga Beli</th>
						<th>Jumlah</th>
						<th>Terjual</th>
						<th>Bayar</th>
						<th>Retur</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				';
				$q="
				SELECT  
					pnd_p_kode,
					pnd_p_nama,
					pnd_p_harga_beli,
					pnd_p_satuan,
					SUM(pnd_titip) as titip,
					SUM(pnd_jumlah) as jumlah,
					SUM(pnd_terjual) as terjual,
					SUM(pnd_bayar) as bayar,
					SUM(pnd_sisa) as sisa,
					pn_status as status
				FROM    
					penitipan,		
					penitipan_detail 
				WHERE
					penitipan.pn_kode 	= penitipan_detail.pnd_pn_kode
					&& pn_tanggal	>= '$sdate'
					&& pn_tanggal	<= '$edate'
					&& pnd_p_k_id	= '$pnd_p_k_id'
				GROUP BY 
					pnd_p_kode
				";
				$res		= mysql_query($q) or die(mysql_error());
				$no			= 0;
				$jbayar		= 0;
				while($r = mysql_fetch_assoc($res)){
					extract($r);
					$no++;
					$status = ($status == 0)? 'On Progress' : 'Done';
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pnd_p_kode.'</b></td>
									<td>'.ucwords($pnd_p_nama).'</td>
									<td>'.$titip.' / '.$pnd_p_satuan.'</td>
									<td>'.rupiah($pnd_p_harga_beli).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($terjual).'</td>
									<td>'.rupiah($bayar).'</td>
									<td>'.$sisa.'</td>
									<td>'.$status.'</td>
								</tr>
								'; 
				$jbayar += $bayar;
				}							
				$content	.= '
							<tr>
								<td colspan="6"></td>
								<td class="success"><b>Jumlah Pembayaran </b></td>
								<td class="success"><b>'.rupiah($jbayar).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							';
			}
			$content	.= '
						<div class="table-responsive">
						<table class="table table-bordered">
						<tr class="success">
							<td ><b>Jumlah Total Pembayaran</b></td>
							<td colspan="8"><b>'.rupiah($gbayar).'</b></td>
						</tr>	
						</table>
						</div>
						<br>
						<br>
						<a data-toggle="tooltip" title="Export Laporan Penitipan (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_penitipan_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
						';
		} else {
			$content .= '
					<h5>Tanggal Pembelian '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th>No</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Titip</th>
							<th>Harga Beli</th>
							<th>Jumlah</th>
							<th>Terjual</th>
							<th>Bayar</th>
							<th>Retur</th>
							<th>Status</th>
					</thead>
					<tbody>
					';
				$no=0;
				$jbayar = 0;
				while($row = mysql_fetch_assoc($result)){
					extract($row);
					$no++;
					$status = ($status == 0)? 'On Progress' : 'Done';
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pnd_p_kode.'</b></td>
									<td>'.ucwords($pnd_p_nama).'</td>
									<td>'.$titip.' / '.$pnd_p_satuan.'</td>
									<td>'.rupiah($pnd_p_harga_beli).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($terjual).'</td>
									<td>'.rupiah($bayar).'</td>
									<td>'.$sisa.'</td>
									<td>'.$status.'</td>
								</tr>
								'; 
				$jbayar += $bayar;
				}
				$content	.= '
							<tr>
								<td colspan="6"></td>
								<td class="success"><b>Jumlah Pembayaran </b></td>
								<td class="success"><b>'.rupiah($jbayar).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							<br>
							<br>
							<a data-toggle="tooltip" title="Export Laporan Penitipan (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_penitipan_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
							';
		}
		
    } else {
		$content    .= '
				<h5>Tanggal Penitipan '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<tbody>
				<tr><td colspan="10" class="text-center"> -- Not Found Content --</td></tr>
				</tbody>
				</table>
				</div>
				';
	}
    echo $content;
?>