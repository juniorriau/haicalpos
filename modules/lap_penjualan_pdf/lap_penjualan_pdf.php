<?php 
defined('_IEXEC') or die('');
	
if(!isset($_REQUEST['sdt']) && !isset($_REQUEST['edt']) && !isset($_REQUEST['sid'])){
	$sdate		= '';
	$edate		= '';
	$kategori_id	= '';
	
}else{
	$sdate		= paramDecrypt($_REQUEST['sdt']);
	$edate		= paramDecrypt($_REQUEST['edt']);
	$kategori_id	= paramDecrypt($_REQUEST['sid']);
}

// Data Company==============================
$query  = "
	SELECT  
		*
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}
//===========================================
	
// Report Detail ============================
	if ($kategori_id<=0){
		$query="
		SELECT  
			pjd_p_k_id
		FROM    
			penjualan,		
			penjualan_detail 
		WHERE
			penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
			&& pj_bayar > 0
		GROUP BY 
			pjd_p_k_id
		";
		$result = mysql_query($query) or die(mysql_error());
	} else {
		$query="
		SELECT  
			pjd_p_k_id,
			pjd_p_kode,
            pjd_p_nama,
            pjd_p_harga_jual,
			pjd_p_satuan,
			SUM(pjd_profit) as profit,
            SUM(pjd_beli) as beli,
            SUM(pjd_jumlah) as jumlah
		FROM    
			penjualan,		
			penjualan_detail 
		WHERE
			penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
			&& pjd_p_k_id	= '$kategori_id'
			&& pj_bayar > 0
		GROUP BY 
			pjd_p_kode
		";
		$result = mysql_query($query) or die(mysql_error());
	}
	
    if(mysql_num_rows($result)>0){
		$content = '';
		if ($kategori_id<=0) {
			$que="
			SELECT  
				SUM(pj_jumlah) as jtotal_all,
				SUM(pj_diskon) as jtdiskon,
				SUM(pj_ppn) as jtppn,
				SUM(pj_total) as gtotal,
				SUM(pj_profit) as jtprofit
			FROM    
				penjualan 
			WHERE
				pj_tanggal		>= '$sdate'
				&& pj_tanggal	<= '$edate'
				&& pj_bayar > 0
			";
			$resu = mysql_query($que) or die(mysql_error());
			$rw = mysql_fetch_assoc($resu);
			extract($rw);
			
			while($row = mysql_fetch_array($result)){
				extract($row);
				$contents .= '
				<h5 style="width:100%; text-align:left;">Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($pjd_p_k_id).')</h5>
				<table class="report">
					<tr class="header">
						<td>No</td>
						<td>Kode</td>
						<td>Nama</td>
						<td>Beli</td>
						<td>Harga Jual</td>
						<td>Jumlah</td>
						<td>Profit</td>
					</tr>
				';
				$q="
				SELECT  
					pjd_p_kode,
					pjd_p_nama,
					pjd_p_harga_jual,
					pjd_p_satuan,
					SUM(pjd_profit) as profit,
					SUM(pjd_beli) as beli,
					SUM(pjd_jumlah) as jumlah
				FROM    
					penjualan,		
					penjualan_detail 
				WHERE
					penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
					&& pj_tanggal	>= '$sdate'
					&& pj_tanggal	<= '$edate'
					&& pjd_p_k_id	= '$pjd_p_k_id'
					&& pj_bayar > 0
				GROUP BY 
					pjd_p_kode
				";
				$res = mysql_query($q) or die(mysql_error());
				$no=0;
				$jtotal = 0;
				$jprofit	= 0;
				while($r = mysql_fetch_assoc($res)){
					extract($r);
					$no++;
					$contents	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pjd_p_kode.'</b></td>
									<td>'.ucwords($pjd_p_nama).'</td>
									<td>'.$beli.'</td>
									<td>'.rupiah($pjd_p_harga_jual).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($profit).'</td>
								</tr>
								'; 
				$jtotal	+= $jumlah;
				$jprofit+= $profit;
				}
				$contents	.= '
							<tr>
								<td colspan="4"></td>
								<td><b>Jumlah Penerimaan Kotor & Profit</b></td>
								<td><b>'.rupiah($jtotal).'</b></td>
								<td><b>'.rupiah($jprofit).'</b></td>
							</tr>
							</table>
							';
			}
			$omtppn		= $jtotal_all-$jtdiskon;
			$contents	.= '
						<br><br><br><br>
						<hr><hr><br>
						<table class="report">
						<tr>
							<td ><b>Total Penerimaan Kotor</b></td>
							<td><b>'.rupiah($jtotal_all).'</b></td>
						</tr>
						<tr>
							<td ><b>Total Profit</b></td>
							<td><b>'.rupiah($jtprofit).'</b></td>
						</tr>	
						<tr>
							<td ><b>Total Pemotongan Diskon</b></td>
							<td><b>'.rupiah($jtdiskon).'</b></td>
						</tr>	
						<tr>
							<td ><b>Total Penerimaan Bersih (Total Penerimaan Kotor - Total Pemotongan Diskon)</b></td>
							<td><b>'.rupiah($omtppn).'</b></td>
						</tr>						
						<tr>
							<td ><b>Total Penerimaan PPn</b></td>
							<td><b>'.rupiah($jtppn).'</b></td>
						</tr>
						<tr>
							<td ><b>Total Penerimaan Keseluruhan (Total Penerimaan Bersih + Total Penerimaan PPn)</b></td>
							<td><b>'.rupiah($gtotal).'</b></td>
						</tr>	
						</table>
						';
		} else {
			$contents .= '
					<h5 style="width:100%; text-align:left;">Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($kategori_id).')</h5>
					<table class="report">
					<tr class="header">
						<td>No</td>
						<td>Kode</td>
						<td>Nama</td>
						<td>Beli</td>
						<td>Harga Jual</td>
						<td>Jumlah</td>
						<td>Profit</td>
					</tr>
					';
				$no=0;
				$jtotal = 0;
				$jprofit	= 0;
				while($row = mysql_fetch_assoc($result)){
					extract($row);
					$no++;
					$contents	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pjd_p_kode.'</b></td>
									<td>'.ucwords($pjd_p_nama).'</td>
									<td>'.$beli.'</td>
									<td>'.rupiah($pjd_p_harga_jual).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($profit).'</td>
								</tr>
								'; 
				$jtotal += $jumlah;
				$jprofit+= $profit;
				}
				$contents	.= '
							<tr>
								<td colspan="4"></td>
								<td><b>Jumlah Penerimaan Kotor & Profit</b></td>
								<td><b>'.rupiah($jtotal).'</b></td>
								<td><b>'.rupiah($jprofit).'</b></td>
							</tr>
							</table>
							';
		}
		
       } else {
		$contents   .= '
				<h5 style="width:100%; text-align:left;">Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($pjd_p_k_id).')</h5>
				<table class="report">
				<tr class="header"><td colspan="7" style="text-align:center;"> -- Not Found Content --</td></tr>
				</table>
				';
	}	
//================================================
$file_name='LAP_PENJUALAN_'.get_category($kategori_id).'_'.showdt($sdate, 2).'-'.showdt($edate, 2).'.pdf';
ob_clean();
ob_start();
?>

<style>
	table.report{
        width:100%;
        border-collapse:collapse;
    }
    table.report tr td{
        padding:5px;
        border:1px solid #000000;
    }
    table.report tr.header td{
        text-align:center;
        font-weight:bold;
        background-color:#B3B1AF;
    }
    table.report tr td.content{
        text-align:right;
        width:20%;
        font-size:10px;
    }
    table.report tr.even td{background-color:#EBE9E8;}
</style>

<page style="font-size:12px;" backtop="10mm" backbottom="10mm">
	<page_footer>
		<table style="width:100%;">			
			<tr>
				<td style="width:35%; text-align:left; font-size:8px;"><?php echo 'LAP_PENJUALAN_'.$sdate.'-'.$edate.'('.get_category($kategori_id).')';?></td>
				<td style="width:30%; text-align:center; font-size:8px;">[[page_cu]]/[[page_nb]]</td>
				<td style="width:35%; text-align:right; font-size:8px;"><i>printed: <?php echo date('Y-m-d, H:i:s');?></i></td>
			</tr>
		</table>
	</page_footer>
	<h3 style="width:100%; text-align:center;"><b><u><?php echo strtoupper($c_nama); ?></u></b></h3>
	<p style="width:100%; text-align:center;">
	<?php echo ucwords($c_alamat); ?><br>
	<?php echo ucwords($c_kontak); ?><br>
	<?php echo ucwords($c_slogan); ?>
	</p>
	<hr>
	<h4 style="text-align:left; width:100%;">LAPORAN PENJUALAN</h4>
	<hr>
	<hr>
	<?php echo $contents;?>
	<hr>
	<hr>
	<table style="border-collapse:collapse; width:100%">
		<tr>	
			<td style="text-align:right;width:100%">Mengetahui,</td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%"><b>(<?php echo strtoupper($c_nama); ?> Management)</b></td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%">SIGNED by System</td>
		</tr>
	</table>
	<hr>
</page>
<?php
	$content = ob_get_clean();

// convert in PDF
require_once('plugins/html2pdf/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P', 'A4', 'en',true, 'UTF-8', array(20, 5, 10, 5));//, true, 'UTF-8', array(10, 7, 30, 5));
	$html2pdf->pdf->SetMargins(10, 7, 30, 5);
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($file_name);
} 
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}