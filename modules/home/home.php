<?php
defined('_IEXEC')or die('');

$d = date("d"); //tanggal sekarang
$m = date("m"); //bulan sekarang
$y = date("Y"); //tahun sekarang

//Jumlah Penjualan
$WhereDay='';
$WhereDay =" && DAY(pj_tanggal) = ".$d." ";

$WhereMonth='';
$WhereMonth =" && MONTH(pj_tanggal) = ".$m." ";

$WhereYear='';
$WhereYear=" YEAR(pj_tanggal) = ".$y." ";
	
$query  = "
	SELECT  
		COUNT(*) as jual
	FROM    
		penjualan 
	WHERE
		".$WhereYear.$WhereMonth.$WhereDay."
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}

//Jumlah Pembelian
$WhereMonth2='';
$WhereMonth2 =" && MONTH(pb_tanggal) = ".$m." ";

$WhereYear2='';
$WhereYear2=" YEAR(pb_tanggal) = ".$y." ";
	
$query2  = "
	SELECT  
		COUNT(*) as beli
	FROM    
		pembelian
	WHERE
		".$WhereYear2.$WhereMonth2."
";
$result2 = mysql_query($query2)or die(mysql_error());
if(mysql_num_rows($result2)){
	extract(mysql_fetch_assoc($result2));
}

//Jumlah Penitipan
$WhereMonth3='';
$WhereMonth3 =" && MONTH(pn_tanggal) = ".$m." ";

$WhereYear3='';
$WhereYear3=" YEAR(pn_tanggal) = ".$y." ";
	
$query3  = "
	SELECT  
		COUNT(*) as titip
	FROM    
		penitipan
	WHERE
		".$WhereYear3.$WhereMonth3."
";
$result3 = mysql_query($query3)or die(mysql_error());
if(mysql_num_rows($result3)){
	extract(mysql_fetch_assoc($result3));
}

//Jumlah Pengeluaran
$WhereDay4='';
$WhereDay4 =" && DAY(pg_tanggal) = ".$d." ";

$WhereMonth4='';
$WhereMonth4 =" && MONTH(pg_tanggal) = ".$m." ";

$WhereYear4='';
$WhereYear4=" YEAR(pg_tanggal) = ".$y." ";
	
$query4  = "
	SELECT  
		COUNT(*) as keluar
	FROM    
		pengeluaran
	WHERE
		".$WhereYear4.$WhereMonth4.$WhereDay4."
";
$result4 = mysql_query($query4)or die(mysql_error());
if(mysql_num_rows($result4)){
	extract(mysql_fetch_assoc($result4));
}
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo ucwords($p);?> <b>P</b>oint <b>O</b>f <b>S</b>ale <b>I</b>ntegrated <b>S</b>ystem</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<p>
			Selamat Datang <b><?php echo $username;?></b> di Aplikasi Point Of Sale Integrated System, silahkan kelola dengan baik dan teliti...
		</p>
		
		<!-- Small boxes (Stat box) -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-red">
			<div class="inner">
			  <h3><?php echo $jual;?></h3>
			  <p>Transaksi Penjualan Hari Ini</p>
			</div>
			<div class="icon">
			  <i class="fa fa-shopping-cart"></i>
			</div>
			<a href="?p=<?php echo paramEncrypt('penjualan');?>" class="small-box-footer">Selengkapnya... <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-green">
			<div class="inner">
			  <h3><?php echo $beli;?></h3>
			  <p>Transaksi Pembelian Bulan Ini</p>
			</div>
			<div class="icon">
			  <i class="fa fa-shopping-cart"></i>
			</div>
			<a href="?p=<?php echo paramEncrypt('pembelian');?>" class="small-box-footer">Selengkapnya... <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-yellow">
			<div class="inner">
			  <h3><?php echo $titip;?></h3>
			  <p>Transaksi Penitipan Bulan Ini</p>
			</div>
			<div class="icon">
			  <i class="fa fa-shopping-cart"></i>
			</div>
			<a href="?p=<?php echo paramEncrypt('penitipan');?>" class="small-box-footer">Selengkapnya... <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-aqua">
			<div class="inner">
			  <h3><?php echo $keluar;?></h3>
			  <p>Transaksi Pengeluaran Bulan Ini</p>
			</div>
			<div class="icon">
			  <i class="fa fa-check-square"></i>
			</div>
			<a href="?p=<?php echo paramEncrypt('pengeluaran');?>" class="small-box-footer">Selengkapnya... <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div><!-- ./col -->
	
	</div><!-- /.box-body -->
	
	<div class="box-footer clearfix">
		<!-- Isi footer-->
	</div><!-- /.box-footer -->
</div><!-- /.box -->
