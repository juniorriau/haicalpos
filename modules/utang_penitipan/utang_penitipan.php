<?php
defined('_IEXEC')or die('');

$q = mysql_query("
	SELECT pnd_p_harga_beli, pnd_titip, p_stok
	FROM
		penitipan
	INNER JOIN
		penitipan_detail
	ON
		pn_kode = pnd_pn_kode
	INNER JOIN
		produk
	ON
		pnd_p_kode = p_kode
	WHERE
		pn_status = 0
")or die(mysql_error());
if(mysql_num_rows($q)){
	$jbayar=0;
	$tbayar='';
	while($r = mysql_fetch_array($q)){
		extract($r);
		$terjual	= $pnd_titip - $p_stok;
		$sisa		= $p_stok;
		$bayar		= $terjual * $pnd_p_harga_beli;
		$jbayar		+= $bayar;
	}				
} else {
	$jbayar=0;
}
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Management <?php echo ucwords($p); ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover" id="mytable">
			<thead>
				<tr class="success">
					<th>#</th>
					<th>Kode</th>
					<th>Tanggal</th>
					<th>Suplier</th>
					<th>No Nota Beli</th>
					<th>Status</th>
					<th>Jumlah Pembayaran</th>
					<th>Input By</th>
					<th>Action</th>
			</thead>
			<tbody id="contents">
			<!-- Isi-->
			</tbody>
		</table>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<a data-toggle="tooltip" title="Kembali ke Penitipan" class="btn btn-success" href="?p=<?php echo paramEncrypt('penitipan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back Penitipan</a>
		<span>Jumlah Total Pembayaran Utang Penitipan <b><?php echo rupiah($jbayar);?></b></span>
	</div><!-- /.box-footer -->
</div><!-- /.box -->	

<script>
// Main Function		
$(function(){ 
	
	function load_data(){
		$("#contents").html('<tr><td colspan="13" class="text-center"><i class="fa fa-spinner fa-spin fa-lg"></i></td></tr>');
		var idresult	= 'contents';
			query   	= '';
		
		//alert(query);
		$.ajax({
			url     : 'modules/utang_penitipan/utang_penitipan_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				var result ="#"+idresult;
				$(result).html(data);
				$("#mytable").dataTable();
			},
			error   : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest gagal: "' + errorThrown + '"');
				return false;
			}
		})
	}
	
	load_data();
	$("#selectshow").change(function(){
        load_data();
    })
	
})// Main Function
</script>