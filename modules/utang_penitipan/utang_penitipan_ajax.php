<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
 	
	$query="
		SELECT  
			*
		FROM    
			penitipan 
		WHERE
			pn_status = 0
		ORDER BY 
			pn_kode 
		DESC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
           extract($row);
            $no++;
			$status = ($pn_status == 0)? 'On Progress' : 'Done';
			$q = mysql_query("
				SELECT pnd_id, pnd_p_harga_beli, pnd_titip, p_stok
				FROM
					penitipan_detail
				INNER JOIN
					produk
				ON
					pnd_p_kode = p_kode
				WHERE
					pnd_pn_kode = '$pn_kode'
			")or die(mysql_error());
			if(mysql_num_rows($q)){
				if($pn_status==0){
					$jbayar=0;
					while($r = mysql_fetch_array($q)){
						extract($r);
						$terjual	= $pnd_titip - $p_stok;
						$sisa		= $p_stok;
						$bayar		= $terjual * $pnd_p_harga_beli;
						$jbayar	+= $bayar;
					}
				} else {
					$jbayar = $pn_total_bayar; 
				}				
			}
            $content.='
                <tr>
					<td>'.$no.'</td>
                    <td><b>'.$pn_kode.'</b></td>
                    <td>'.showdt($pn_tanggal, 2).'</td>
					<td>'.$pn_suplier.'</td>
					<td>'.$pn_nota.'</td>
					<td>'.$status.'</td>
					<td>'.rupiah($jbayar).'</td>
					<td>'.get_fullname($pn_user_id).'</td>
					<td>
						<a data-toggle="tooltip" title="Lakukan Pembayaran Penitipan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('penitipan_form').'&kd='.paramEncrypt($pn_kode).'&pndid='.paramEncrypt(0).'" role="button"><i class="fa fa-edit"></i></a>
					</td>
                </tr>
				';
			//tombol delete
			//<a class="btn btn-xs btn-danger" href="javascript:del('.$pj_kode.')" role="button"><i class="fa fa-trash-o"></i></a>
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/penitipan_form/penitipan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	window.alert("Data Berhasil Dihapus");    
				window.location = '?p=<?php echo paramEncrypt('penjualan');?>';
			}
		});
	}
}
</script>