<?php 
defined('_IEXEC') or die('');
	
if(!isset($_REQUEST['sdt']) && !isset($_REQUEST['edt'])){
	$sdate		= '';
	$edate		= '';
	$stant_id	= '';
	
}else{
	$sdate		= paramDecrypt($_REQUEST['sdt']);
	$edate		= paramDecrypt($_REQUEST['edt']);
}

// Data Company==============================
$query  = "
	SELECT  
		*
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}
//===========================================
	
// Report Detail ============================
	$query="
	SELECT  
		*
	FROM    
		pengeluaran 
	WHERE
		pg_tanggal	>= '$sdate'
		&& pg_tanggal	<= '$edate'
	ORDER BY 
		pg_kode
	ASC	
	";
	$result = mysql_query($query) or die(mysql_error());
	
	$contents = '';
    if(mysql_num_rows($result)>0){			
		$contents .= '
		<h5 style="width:100%; text-align:left;">Periode Pengeluaran '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
		<table class="report">
			<tr class="header">
				<td>#</td>
				<td>Kode</td>
				<td>Tanggal</td>
				<td>Penanggung Jawab</td>
				<td>Jenis Pengeluaran</td>
				<td>Jumlah</td>					
				<td>Keterangan</td>
				<td>Input By</td>
			</tr>
		';
		
		$no			= 0;
		while($r = mysql_fetch_assoc($result)){
			extract($r);
			$no++;
			$contents	.='
						<tr class="content">
							<td>'.$no.'</td>
							<td><b>'.$pg_kode.'</b></td>
							<td>'.showdt($pg_tanggal, 2).'</td>
							<td>'.$pg_nama.'</td>
							<td>'.$pg_jenis.'</td>
							<td>'.rupiah($pg_jumlah).'</td>
							<td>'.$pg_keterangan.'</td>
							<td>'.get_fullname($pg_user_id).'</td>
						</tr>
						'; 	
		}			
		
		$contents	.= '
		</table>
		';
		
    } else {
		$contents    .= '
				<h5 style="width:100%; text-align:left;">Periode Pengeluaran '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
				<table class="report">
				<tr class="content"><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>
				</table>
				';
	}
//================================================
$file_name='LAP_PENGELUARAN_'.$sdate.'-'.$edate.'.pdf';
ob_clean();
ob_start();
?>

<style>
	table.report{
        width:100%;
        border-collapse:collapse;
    }
    table.report tr td{
        padding:5px;
        border:1px solid #000000;
    }
    table.report tr.header td{
        text-align:center;
        font-weight:bold;
        background-color:#B3B1AF;
    }
    table.report tr td.content{
        text-align:right;
        width:20%;
        font-size:10px;
    }
    table.report tr.even td{background-color:#EBE9E8;}
</style>

<page style="font-size:10.2px;" backtop="10mm" backbottom="10mm">
	<page_footer>
		<table style="width:100%;">			
			<tr>
				<td style="width:35%; text-align:left; font-size:8px;"><?php echo 'LAP_PENGELUARAN_'.$sdate.'-'.$edate.'';?></td>
				<td style="width:30%; text-align:center; font-size:8px;">[[page_cu]]/[[page_nb]]</td>
				<td style="width:35%; text-align:right; font-size:8px;"><i>printed: <?php echo date('Y-m-d, H:i:s');?></i></td>
			</tr>
		</table>
	</page_footer>
	<h3 style="width:100%; text-align:center;"><b><u><?php echo strtoupper($c_nama); ?></u></b></h3>
	<p style="width:100%; text-align:center;">
	<?php echo ucwords($c_alamat); ?><br>
	<?php echo ucwords($c_kontak); ?><br>
	<?php echo ucwords($c_slogan); ?>
	</p>
	<hr>
	<h4 style="text-align:left; width:100%;">Laporan Pengeluaran</h4>
	<hr>
	<hr>
	<?php echo $contents;?>
	<hr>
	<hr>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<hr>
	<table style="border-collapse:collapse; width:100%">
		<tr>	
			<td style="text-align:right;width:100%">Mengetahui,</td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%"><b>(<?php echo strtoupper($c_nama); ?> Management)</b></td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%">SIGNED by System</td>
		</tr>
	</table>
	<hr>
</page>
<?php
	$content = ob_get_clean();

// convert in PDF
require_once('plugins/html2pdf/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P', 'A4', 'en',true, 'UTF-8', array(20, 7, 10, 5));//, true, 'UTF-8', array(10, 7, 30, 5));
	//$html2pdf->pdf->SetMargins(10, 7, 30, 5);
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($file_name);
} 
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}