<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$month  = $_POST['m'];
	$year   = $_POST['y'];
	$show	= $_POST['show'];      // jumlah record per page
	$limit  = ($show)? ' LIMIT '.$show:''; // limit pada seleksi data
    
	$WhereMonth='';
	if(intval($month) > 0){
		$WhereMonth =" && MONTH(rpj_tanggal) = ".$month." ";
	}

	$WhereYear='';
	if(intval($year) > 0){
		$WhereYear=" YEAR(rpj_tanggal) = ".$year." ";
	}
	
	$query="
		SELECT  
			*
		FROM    
			retur_penjualan 
		WHERE
		".$WhereYear.$WhereMonth."
		ORDER BY 
			rpj_kode 
		ASC ". $limit;
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="9" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
					<td><b>'.$rpj_kode.'</b></td>
					<td>'.showdt($rpj_tanggal, 2).'</td>
                    <td><b>'.$rpj_pj_kode.'</b></td>
                    <td>'.rupiah($rpj_jumlah).'</td>
					<td>'.rupiah($rpj_potongan).'</td>
					<td>'.rupiah($rpj_total).'</td>
					<td>'.get_fullname($rpj_user_id).'</td>
			';
			if($rpj_total==0) {
				$content.= '
					<td>
						<a data-toggle="tooltip" title="Edit Retur Penjualan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('retur_penjualan_form').'&kdpj='.paramEncrypt($rpj_pj_kode).'&kdrpj='.paramEncrypt($rpj_kode).'&pjdid='.paramEncrypt(0).'" role="button"><i class="fa fa-edit"></i></a>
					</td>
				</tr>
			';
			} else {
				$content.= '
					<td>
						...	
					</td>
				</tr>
			';
			}
        }
    }
    echo $content;
?>