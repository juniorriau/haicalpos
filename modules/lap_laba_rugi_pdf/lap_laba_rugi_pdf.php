<?php 
defined('_IEXEC') or die('');
	
if(!isset($_REQUEST['sdt']) && !isset($_REQUEST['edt'])){
	$sdate		= '';
	$edate		= '';
	$stant_id	= '';
	
}else{
	$sdate		= paramDecrypt($_REQUEST['sdt']);
	$edate		= paramDecrypt($_REQUEST['edt']);
}

// Data Company==============================
$query  = "
	SELECT  
		*
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}
//===========================================
	
// Report Detail ============================
		$q1 = mysql_query("
		SELECT SUM(pj_total) as pj_total
		FROM
			penjualan
		WHERE
			pj_bayar>0
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q1)){
		extract(mysql_fetch_array($q1));
	} else {
		$pj_total = 0;
	}

		$q2a = mysql_query("
		SELECT SUM(pb_bayar) as bayar1
		FROM
			pembelian
		WHERE
			pb_status = 1
			&& pb_sisa_bayar = 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2a)){
		extract(mysql_fetch_array($q2a));
	} else {
		$bayar1 = 0;
	}

	$q2b = mysql_query("
		SELECT SUM(pb_sisa_bayar) as bayar2
		FROM
			pembelian
		WHERE
			pb_status = 1
			&& pb_sisa_bayar > 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2b)){
		extract(mysql_fetch_array($q2b));
	} else {
		$bayar2 = 0;
	}

	$q2c = mysql_query("
		SELECT SUM(pb_bayar) as bayar3
		FROM
			pembelian
		WHERE
			pb_status = 0
			&& pb_sisa_bayar > 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2c)){
		extract(mysql_fetch_array($q2c));
	} else {
		$bayar3 = 0;
	}

	$pb_bayar = $bayar1+$bayar2+$bayar3;

	$q3 = mysql_query("
		SELECT SUM(pn_total_bayar) as pn_total_bayar
		FROM
			penitipan
		WHERE
			pn_status = 1
			&& pn_tanggal	>= '$sdate'
			&& pn_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q3)){
		extract(mysql_fetch_array($q3));
	} else {
		$pn_total_bayar = 0;
	}

	$q4 = mysql_query("
		SELECT SUM(pg_jumlah) as pg_jumlah
		FROM
			pengeluaran
		WHERE
			pg_tanggal	>= '$sdate'
			&& pg_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q4)){
		extract(mysql_fetch_array($q4));
	} else {
		$pg_jumlah = 0;
	}

	$tpengeluaran	= $pb_bayar + $pn_total_bayar + $pg_jumlah;
	$saldo			= $pj_total - $tpengeluaran;
	
	$contents = '';	
	$contents .= '
	<h5 style="width:100%; text-align:left;">Periode Laba Rugi '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
	<table class="report">
		<tr class="header">
			<td colspan="2">Pendapatan Usaha</td>
		</tr>
		<tr>
			<td>Penjualan</td>
			<td>'.rupiah($pj_total).'</td>
		</tr>
		<tr class="header">
			<td>Jumlah Pendapatan Usaha</td>
			<td>'.rupiah($pj_total).'</td>
		</tr>
	</table><br><br>
	<table class="report"">
		<tr class="header">
			<td colspan="2">Pengeluaran Operasional</td>
		</tr>
		<tr>
			<td>Pembelian</td>
			<td>'.rupiah($pb_bayar).'</td>
		</tr>
		<tr>
			<td>Pembayaran Penitipan</td>
			<td>'.rupiah($pn_total_bayar).'</td>
		</tr>
		<tr>
			<td>Pengeluaran Umum</td>
			<td>'.rupiah($pg_jumlah).'</td>
		</tr>
		<tr>
			<td>Pembelian + Pembayaran Penitipan + Pengeluaran Umum ('.rupiah($pb_bayar).' + '.rupiah($pn_total_bayar).' + '.rupiah($pg_jumlah).')</td>
			<td>'.rupiah($tpengeluaran).'</td>
		</tr>
		<tr class="header">
			<td>Jumlah Pengeluaran Operasional</td>
			<td>'.rupiah($tpengeluaran).'</td>
		</tr>
	</table><br><br>
	<table class="report">
		<tr class="header">
			<td colspan="2">Saldo Pendapatan</td>
		</tr>
		<tr>
			<td>Pendapatan Usaha - Pengeluaran Operasional ('.rupiah($pj_total).' - '.rupiah($tpengeluaran).')</td>
			<td>'.rupiah($saldo).'</td>
		</tr>
		<tr class="header">
			<td>Jumlah Saldo Pendapatan</td>
			<td>'.rupiah($saldo).'</td>
		</tr>
	</table>
	';
		
//================================================
$file_name='LAP_LABA_RUGI_'.$sdate.'-'.$edate.'.pdf';
ob_clean();
ob_start();
?>

<style>
	table.report{
        width:100%;
        border-collapse:collapse;
    }
    table.report tr td{
        padding:5px;
        border:1px solid #000000;
    }
    table.report tr.header td{
        text-align:center;
        font-weight:bold;
        background-color:#B3B1AF;
    }
    table.report tr td.content{
        text-align:right;
        width:20%;
        font-size:10px;
    }
    table.report tr.even td{background-color:#EBE9E8;}
</style>

<page style="font-size:12px;" backtop="10mm" backbottom="10mm">
	<page_footer>
		<table style="width:100%;">			
			<tr>
				<td style="width:35%; text-align:left; font-size:8px;"><?php echo 'LAP_LABA_RUGI_'.$sdate.'-'.$edate.'';?></td>
				<td style="width:30%; text-align:center; font-size:8px;">[[page_cu]]/[[page_nb]]</td>
				<td style="width:35%; text-align:right; font-size:8px;"><i>printed: <?php echo date('Y-m-d, H:i:s');?></i></td>
			</tr>
		</table>
	</page_footer>
	<h3 style="width:100%; text-align:center;"><b><u><?php echo strtoupper($c_nama); ?></u></b></h3>
	<p style="width:100%; text-align:center;">
	<?php echo ucwords($c_alamat); ?><br>
	<?php echo ucwords($c_kontak); ?><br>
	<?php echo ucwords($c_slogan); ?>
	</p>
	<hr>
	<h4 style="text-align:left; width:100%;">Laporan Laba Rugi</h4>
	<hr>
	<hr>
	<?php echo $contents;?>
	<hr>
	<hr>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<hr>
	<table style="border-collapse:collapse; width:100%">
		<tr>	
			<td style="text-align:right;width:100%">Mengetahui,</td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%"><b>(<?php echo strtoupper($c_nama); ?> Management)</b></td>
		</tr>
		<tr>	
			<td style="text-align:right;width:100%">SIGNED by System</td>
		</tr>
	</table>
	<hr>
</page>
<?php
	$content = ob_get_clean();

// convert in PDF
require_once('plugins/html2pdf/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P', 'A4', 'en',true, 'UTF-8', array(20, 7, 10, 5));//, true, 'UTF-8', array(10, 7, 30, 5));
	//$html2pdf->pdf->SetMargins(10, 7, 30, 5);
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($file_name);
} 
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}