<?php

//Koneksi database
include('core/cukang.inc.php');
//Library
include('core/core.php');

$query  = "
	SELECT 
		c_nama
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
} else {
	$c_nama			= '';
}

$p = isset($_REQUEST['p'])? $_REQUEST['p'] : 0;
$msg = '';
if($p){
	switch(paramDecrypt($p)){			
		case 'failed' :
			$msg = '<p class="login-box-msg text-red">username atau password salah !</p>';
			break;
		case 'logout':
			$msg = '<p class="login-box-msg text-green">Terima Kasih</p>';
			break;
		default : $msg = '';
	}
}

$query  = "
	SELECT  user_foto
	FROM
		user
	WHERE
		user_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="noindex">
    <title><?php echo strtoupper($c_nama)?> POSIS | LOG IN</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
	<!-- Favicon -->
	<link rel="shortcut icon" href="images/favicon.ico" />
	
	<!--============CSS=============-->
	<!-- Bootstrap 3.3.5 -->    
	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link type="text/css" rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<!-- Theme style -->
	<link type="text/css" rel="stylesheet" href="dist/css/AdminLTE.min.css">    
	<!-- AdminLTE Skins. Choose a skin from the css/skins
		 folder instead of downloading all of them to reduce the load. -->
	<link type="text/css" rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
	<!--============JavaScript=============-->
	<!-- jQuery 2.1.4 -->
	<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

  </head>
  <body class="hold-transition login-page">
    <div id="modal-info" class="modal" data-backdrop="static">
		<div id="modal-content" class="modal-dialog modal-lg" role="document">        
							
				<div class="box box-solid box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Information <b>P</b>oint <b>O</b>f <b>S</b>ale <b>I</b>ntegrated <b>S</b>ystem</h3>
						<div class="box-tools pull-right">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
					</div>			
					<div class="box-body">
						<p>
							Aplikasi ini adalah sistem aplikasi penjualan terintegrasi (point of sale integrated system) khusus untuk Haikal Store (Pusat oleh-oleh Garut) dibuat dan dikembangkan oleh <b>Danang Eko Alfianto.</b><br>
							Apabila menemukan kesalahan aplikasi (bugs) atau membutuhkan perubahan alur sistem aplikasi, silahkan hubungi kontak di bawah ini:<br>
							Website: <a href="http://danang-ekal.com" target="_blank">danang-ekal.com</a><br>
							Email : danangekal@gmail.com<br>
							Hp : 085624503065/089655797756<br>
							Fb : Danang Eko Alfianto<br>
							BBM: 5CC1240A
						</p>
						<img src="images/users/<?php echo $user_foto; ?>" class="img-circle img-thumbnail"/>
					</div>
					<div class="box-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					</div>
				</div>			
						
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="login-box">
      <div class="login-logo" style="font-size:20px;">
        <a href="#">
			<img src="images/logo.jpg" alt="logo"/>
			<b>P</b>oint <b>O</b>f <b>S</b>ale <b>I</b>ntegrated <b>S</b>ystem
		</a>		
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Silakan login untuk memulai sistem</p>
		<?php echo $msg; ?>
        <form action="haical/check" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Username" required="true" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>			
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required="true">			
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">    
            <div class="col-xs-8">
				<a href="index.php" class="btn btn-success" role="button"><i class="fa fa-home"></i> Home</a>
				<button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
	  
	  <div class="login-box-footer">
			<center><strong>Copyright &copy; 2016 <?php echo ucwords($c_nama);?><br> By <a href="#modal-info" data-toggle="modal">danang-ekal</a></strong></center>
	  </div><!-- /.box-footer -->
    </div><!-- /.login-box -->
	
  </body>
</html>
